\section{Cooperative transportation system}
\label{cha:system}

This section describes the proposed cooperative transportation system in two parts: In Section \ref{sc:system_design}, the hardware architecture of the system and the architecture of the drone control system is described. The technique we utilized is described in Section \ref{sc:method} in more detail.

\subsection{Dual-drone transportation system hardware architecture}
\label{sc:system_design}

Figure \ref{fig:drones_and_load} shows the system’s hardware architecture in this study consisting of two drones, two cables, and one load. The drones use a quadrotor housing at the bottom and a camera overlooking the load. The load length is greater than the maximum width of a drone and is marked as a square on both sides in a black outer frame with a black block and a white background at the center. The cables were used to connect the drones to the load, and the drone end was attached to the drone’s center of gravity, while the two cables at the end of the load were connected on both sides of the load to the marker centers.

\begin{figure} [!t]
    \begin{center}
        \includegraphics[width=85mm]{figures/drones_and_load}
        \caption{Dual-drone transportation system hardware architecture} 
        \label{fig:drones_and_load}
    \end{center}
\end{figure}

\subsubsection{Marker}

The black outer frame containing square markers of images is widely used in augmented reality (AR), where the outer frame of the square markers has four corners, whose position in the image can be used to calculate the relative position and angle between the markers and camera. If the image in the marker is directional, a more accurate attitude of the marker relative to the camera can be obtained. Generally, the image is not directional if it can be restored to the original one by a rotation of $\theta$ degrees ($0<\theta<360$). The relative direction between the markers and camera is calculated from the direction of the image, which, together with the angle obtained through the four corners, can enable us to obtain the marker attitude relative to the camera. In addition to position and attitude, the images in the marker may provide information about the system, with different images representing different objects. Two-dimensional quick response code can also be used to represent information contained in the markers rather than the database.

In addition to detecting the load position in our system, we also need to estimate the load attitude. Therefore, the square marker with a black outer frame was used, and a $4\times4$ directional square matrix with black blocks was used in the middle image, which allowed the drone to detect the markers through the camera overlooking the load. With this, the position and attitude of the markers was obtained and transformed into the load state.

\subsubsection{Cable limitation}



The system cannot monitor the load status if the marker is out of the camera image. The short cable make the marker too close to the camera, or the marker is oversized beyond the camera’s shooting range. So we limite the cable length $l$ as follows:

\begin{equation}
    \label{eq:cable_length}
    \begin{aligned}
        l > (\omega + \frac{w_m}{2}) \times \tan(\frac{{AoV}_{min}}{2})
    \end{aligned}
    % \phantom{\hspace{6cm}}
\end{equation}

\noindent where $\omega$ is the shortest distance between the center of the camera image and image boundary, which prevents swinging movement during the transportation. The swinging movement could cause the marker to go beyond the camera shooting range. $w_m$ is the side length of the marker, and ${AoV}_{min}$ is the smallest angle of view of the camera.

\subsubsection{Drone control system}
\label{subsc:drone_control_system}

\begin{figure} [!t]
    \begin{center}
        \includegraphics[width=85mm]{figures/architecture-drone_control_system_architecture_en}
        \caption{Drone control system architecture} 
        \label{fig:drone_control_system_architecture}
    \end{center}
\end{figure}

The drone control system, as shown in Figure \ref{fig:drone_control_system_architecture}, was a closed-loop system. The main objective is to keep the load state within a stable range, and by controlling the drone to transfer and adjust the load to the target position and attitude. The control system determined how the drone moved based on the feedback drone state and load state. The control system proposed by us was mainly divided into the following three parts:

\begin{itemize}
    \item Marker Detector: it estimates the state of the marker on the load in the drone reference coordinate system $D$ by detecting the image of the camera overlooking the load, as shown in Figure \ref{fig:marker_detector}.
    \item State Estimator: it calculates the state of the load in the inertial coordinate system $I$ based on the marker state estimated by the state estimator and marker estimator of the drone.
    \item PID Controller: it gives the drone relative motion commands based on the target position and attitude of the load and the feedback current state of the drone, marker, and load.
\end{itemize}

\begin{figure} [!t]
    \centering
    \subfloat[Camera overlooking the load]{
        \includegraphics[width=1.2in]{figures/marker_detector}
        \label{fig:marker_detector:a}
    }%
    \subfloat[Camera image]{
        \includegraphics[width=1.2in]{figures/marker_detector_image}
        \label{fig:marker_detector:b}
    }
    \caption{Marker Detector}
    \label{fig:marker_detector}
\end{figure}

The system mainly used two coordinate systems (Fig. \ref{fig:coordinate_systems}): the inertial coordinate system $I$ of the experimental environment and the drone reference coordinate system $D$ with the center of gravity of the drone as the origin. The position and attitude we considered could be expressed by a six-degree-of-freedom equation $p = \begin{pmatrix} x,y,z,\phi,\theta,\psi \end{pmatrix}$, where $x$, $y$, and $z$ indicate the coordinates of the position and $\phi$, $\theta$, and $\psi$ represent three axial rotation angles (roll, pitch, and yaw).

The marker states, including the position and attitude $p_m^D$ of the marker $m$ in the drone reference coordinate system $D$, three axial speeds $\dot{x}_m^D$, $\dot{y}_m^D$, and $\dot{z}_m^D$ and rotational speed $\dot{\psi}_m^D$, were generated by the marker detector. The state estimator estimated the position and attitude $p_l$ of the load $l$ in the experimental environment, three axial speeds $\dot{x}_l$, $\dot{y}_l$, and $\dot{z}_l$, and the speed of rotation $\dot{\psi}_l$ based on the results from the marker detector and the states of the drone in the inertial coordinate system $I$ of the experimental environment (including the position and attitude $p_d$, three axial speeds $\dot{x}_d$, $\dot{y}_d$, and $\dot{z}_d$ and the speed of rotation $\dot{\psi}_d$). The PID Controller generated commands related to three axial speeds $v_x$, $v_y$, and $v_z$ and rotational speed $v_\psi$ to control the drone based on the target load position and attitude $p_l^d$ and the states of the load, markers, and drone. The symbols are listed in Table \ref{tbl:symbols}.

\begin{figure} [!t]
    \begin{center}
        \includegraphics[width=70mm]{figures/coordinate_system}
        \caption{Coordinate systems} 
        \label{fig:coordinate_systems}
    \end{center}
\end{figure}

\begin{table*} [!t]
    \renewcommand{\arraystretch}{1.3}
    \caption{Symbols}
    \label{tbl:symbols}
    \centering
    \scalebox{1.0}{\input{tables/symbols_en.tex}}
\end{table*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Transportation method for non-communicating dual-drone suspended load}
\label{sc:method}

In our system gave the drone the load transporting path, including multiple target load positions and attitudes $p_l^d$. In the flight task, the drone relied on the control system to approximate each target attitude. The drone would determine the states of the load and the other drone to ensure consistent task progress of the two drones.

The task flow of the drone is shown in Figure \ref{fig:workflow}, which mainly consisted of the following three steps: First, the drone used the marker detector to detect the position and attitude of the marker on the load in the camera and then estimated the state of the load according to the position and attitude of the drone. Next, the drone determined whether the position and attitude of the load had reached the target counterpart and the task progress of the other drone through the load state to determine whether it should perform the task to achieve the next target condition. Finally, the command to keep the load state stable during the transportation was created by the PID Controller.

\begin{figure} [!t]
    \begin{center}
        \includegraphics[width=70mm]{figures/architecture-workflow_en}
        \caption{Task flow of the drone} 
        \label{fig:workflow}
    \end{center}
\end{figure}

\subsubsection{Load state estimation}
\label{subsc:load_state_estimate}

Four key steps were taken to estimate the load states. First, to obtain the position and attitude of the marker in the image \cite{artoolkit}. With this, we could obtain the position and attitude of the marker $p_m^D$ in $D$ because the camera position was fixed. Then the position and attitude of the $p_m^D$ marker could be converted into $p_m$ in $I$ by the drone’s attitude. Finally, it was possible to obtain the $p_l$ load position and attitude based on $p_m$ and the marker position on the load.

To transform the position and attitude of the marker detected from the image into its load counterpart, we performed a series of position and attitude transformations, where $p$ was first transformed into a $4\times4$ position and attitude matrix through $M(x,y,z,\phi,\theta,\psi)$ to transform the position and attitude $p$ in the coordinate system $D$ into those in $I$

\begin{equation}
    \label{eq:m_p}
    \begin{aligned}
        M(x,y,z,\phi,\theta,\psi) = 
        \begin{bmatrix}
            \begin{matrix}
                R(\phi,\theta,\psi)
            \end{matrix}
            &
            \begin{matrix}
                x \\ y \\ z
            \end{matrix}
            \\
            \begin{matrix}
                0 & 0 & 0
            \end{matrix}
            &
            \begin{matrix}
                1
            \end{matrix}
        \end{bmatrix}
    \end{aligned}
    % \phantom{\hspace{6cm}}
\end{equation}

\noindent where $R$, a $3 \times 3$ rotation matrix, could be obtained from three axial rotation angles.

\begin{equation}
    \label{eq:r_rzryrx}
    \begin{aligned}
        &R(\phi,\theta,\psi) \\
        &=
        \begin{bmatrix}
            \cos(\psi) & -\sin(\psi) & 0 \\
            \sin(\psi) & \cos(\psi) & 0 \\
            0 & 0 & 1 \\
        \end{bmatrix}
        \times
        \begin{bmatrix}
            \cos(\theta) & 0 & \sin(\theta) \\
            0 & 1 & 0 \\
            -\sin(\theta)  & 0 & \cos(\theta)  \\
        \end{bmatrix} \\
        &\times
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & \cos(\phi) & -\sin(\phi) \\
            1 & \sin(\phi) & \cos(\phi) \\
        \end{bmatrix} \\
        % &=
        % \begin{bmatrix}
        %     \cos(\psi)\cos(\theta) & \cos(\psi)\sin(\theta)\sin(\phi)-\sin(\psi)\cos(\phi) & \cos(\psi)\sin(\theta)\cos(\phi)+\sin(\psi)\sin(\phi) \\
        %     \sin(\psi)\cos(\theta) & \sin(\psi)\sin(\theta)\sin(\phi)+\cos(\psi)\cos(\phi) & \sin(\psi)\sin(\theta)\cos(\phi)-\cos(\psi)\sin(\phi) \\
        %     -\sin(\theta) & \cos(\theta)\sin(\phi) & \cos(\theta)\cos(\phi) \\
        % \end{bmatrix}
    \end{aligned}
    % \phantom{\hspace{6cm}}
\end{equation}

There were two coordinate system transformations in our approach. The first involved the transformation of the camera reference coordinate system $C$ into $D$, which could be achieved using the $T_C^D=M(P_C^D)$ transformation matrix. The other involved the transformation of $D$ into $I$, which could be achieved using the transformation matrix $T_D^I=M(P_d)$. Note that both transformation matrices were based on \ref{eq:m_p}. The above two transformation methods were applied in steps 2 and 3. In addition to the coordinate system transformation, the fourth step also allows the transformation matrices to be used to convert the attitudes on an object at different positions.

In step 2, the position and attitude $p_m^C$ of the marker relative to the camera could be obtained from the image of the camera overlooking the load, which could be transformed into the position and attitude $p_m^D$ of the marker in $D$ through the position and attitude matrix and the transformation matrix $T_C^D$ obtained from the position and attitude $p_C^D$ of the camera on the drone.

\begin{equation}
    \label{eq:marker_pose_in_drone}
    \begin{aligned}
        M(p_m^D) = T_C^D \times M(p_m^C)
    \end{aligned}
    % \phantom{\hspace{6cm}}
\end{equation}

Next, in step 3, the transformation matrix $T_D^I$ obtained from the position and attitude $p_d$ of the drone in $I$ was used to transform $p_m^D$ into the position and attitude $p_m$ of the marker in the coordinate system $I$:

\begin{equation}
    \label{eq:marker_pose}
    \begin{aligned}
        M(p_m) = T_D^I \times M(p_m^D)
    \end{aligned}
    % \phantom{\hspace{6cm}}
\end{equation}

Finally, in step 4, the transformation matrix $T_m^l$ was obtained through the position of the marker on the load to transform the position and attitude $p_m$ of the marker into the load counterpart $p_l$.

\begin{equation}
    \label{eq:load_pose}
    \begin{aligned}
        M(p_l) = M(p_m) \times {T_m^l}^{-1}
    \end{aligned}
    % \phantom{\hspace{6cm}}
\end{equation}

\subsubsection{PID Controller}
\label{subsc:pid_controller}

\begin{figure} [!t]
    \begin{center}
        \includegraphics[width=85mm]{figures/architecture-pid_controller_architecture_zh-hant}
        \caption{PID Controller architecture} 
        \label{fig:pid_controller_architecture}
    \end{center}
\end{figure}

The PID Controller, a primary controller in a closed-loop system, was used to generate commands to control the drone. A fixed-gain PID Controller could simplify the dynamics model to converge the dynamic state to the desired equilibrium one. We divide the PID Controller into three parts: horizontal movement, vertical movement, and rotation (shown in Figure\ref{fig:pid_controller_architecture}). It generated horizontal movement, vertical movement, and rotation commands according to the position and attitude of the target load, as well as the states of the drone, marker, and load.

First the error between the current state $p_l$ and the target position and attitude $p_l^d$ of the load was calculated to change the position and attitude of the load toward its target counterpart. The error $p_{err}$ could be expressed as

\begin{alignat}{2}
    p_{err} &= p_l^d - p_l
    \label{eq:pid_controller_load_pose_err} \\
    \notag \\
    p_{err} &= 
    \begin{pmatrix}
        x_{err},y_{err},z_{err},\phi_{err},\theta_{err},\psi_{err}
    \end{pmatrix}
    \label{eq:pid_controller_load_pose_err2} \\
    \notag
\end{alignat}

The horizontal PID Controller was mainly responsible for the horizontal movement of the drone, where the commands of horizontal movement speeds $v_x$ and $v_y$, expressed below, were created using the feedback information.

\begin{alignat}{2}
    &v_x(t) \notag \\
    &= K_P^{xy} (s_{xy} x^D_{err}(t) + s_r \psi_{err}(t) + s_m x^D_m(t)) \notag \\
    &+ K_I^{xy} \int_0^t (s_{xy} x^D_{err}(T) + s_r \psi_{err}(T) + s_m x^D_m(T)) dT
    \label{eq:pid_controller_vx} \\
    &+ K_D^{xy} (s_{xy} \dot{x}_l^D(t) + s_r \dot{\psi}_l(t) + s_m \dot{x}^D_m(t)) \notag \\
    % K_D^{xy} \frac{d}{dt} (s_{xy} x^D_{err}(t) + s_r \psi_{err}(t) + s_m x^D_m(t))
    \notag \\
    &v_y(t) \notag \\
    &= K_P^{xy} (s_{xy} y^D_{err}(t) + s_m y^D_m(t)) \notag \\
    &+ K_I^{xy} \int_0^t (s_{xy} y^D_{err}(T) + s_m y^D_m(T)) dT
    \label{eq:pid_controller_vy} \\
    &+ K_D^{xy} (s_{xy} \dot{y}_l^D(t) + s_m \dot{y}^D_m(t)) \notag \\
    % K_D^{xy} \frac{d}{dt} (s_{xy} y^D_{err}(t) + s_m y^D_m(t))
    \notag
\end{alignat}

$x^D_{err}$, $y^D_{err}$, $\dot{x}_l^D$, and $\dot{y}_l^D$ represent the errors in the horizontal positions and horizontal movement speeds of the load in the drone reference coordinate system. Through the yaw angle of the drone $\psi_d$, the parameters $x_{err}$, $y_{err}$, $\dot{x}_p$, and $\dot{y}_p$ in $I$ could be transformed into those in $D$:

\begin{alignat}{2}
    x^D_{err} &= cos(\psi_d) \times x_{err} + sin(\psi_d) \times y_{err}
    \label{eq:pid_controller_load_x_err_to_drone_coordinate} \\
    \notag \\
    y^D_{err} &= -sin(\psi_d) \times x_{err} + cos(\psi_d) \times y_{err}
    \label{eq:pid_controller_load_y_err_to_drone_coordinate} \\
    \notag \\
    \dot{x}_l^D &= cos(\psi_d) \times \dot{x}_l + sin(\psi_d) \times \dot{y}_l
    \label{eq:pid_controller_load_x_speed_to_drone_coordinate} \\
    \notag \\
    \dot{y}_l^D &= -sin(\psi_d) \times \dot{x}_l + cos(\psi_d) \times \dot{y}_l
    \label{eq:pid_controller_load_y_speed_to_drone_coordinate_y} \\
    \notag
\end{alignat}

The load could be moved to the target position by correcting errors in the load horizontal position. By correction of $\psi_{err}$, the feedback error in load yaw, the controller could rotate the load to the target yaw angle according to different flight directions of the two drones. $x^D_m$, $y^D_m$, representing the horizontal positions of the marker in the drone reference coordinate system, and approximately representing the swing amplitude of the cable-suspended load, could be adjusted to reduce the swing magnitude of the suspended load.

The vertical PID Controller corrected the error $z_{err}$ in the vertical position and error $\phi_{err}$ in rollover of the load by up-and-down movement of the drone, and the horizontal PID Controller corrected the yaw error in a similar way, where the difference in vertical movement directions of the two drones was used to control the rollover angle of the load to the target value, with the vertical movement speed command $v_z$ expressed as

\begin{equation}
    \label{eq:pid_controller_vz}
    \begin{aligned}
        v_z(t)
        &=
        K_P^z (s_z z_{err}(t) + s_r \phi_{err}(t)) \\
        &+
        K_I^z \int_0^t (s_z z_{err}(T) + s_r \phi_{err}(T)) dT \\
        &+
        K_D^z \dot{z}_l \\
        % K_D^z \frac{d}{dt} (s_z z_{err}(t) + s_r \phi_{err}(t)) \\
    \end{aligned}
    % \phantom{\hspace{6cm}}
\end{equation}

The rotary PID Controller was responsible for controlling the yaw angle of the drone, which mainly aimed to control the drone to be exactly directed in the direction of the marker on the load under the drone; this reduced the complexity for the horizontal PID Controller to adjust the yaw angle of the load. The speed command for the yaw rotation of the drone, $v_\psi$, can be expressed as

\begin{equation}
    \label{eq:pid_controller_vyaw}
    \begin{aligned}
        v_\psi(t)
        =
        K_P^\psi \psi^{D}_m(t) + K_I^\psi \int_0^t \psi^{D}_m(T)dT + K_D^\psi \dot{\psi}^{D}_m(t)
        % \frac{d}{dt} \psi^{D}_m(t)
    \end{aligned}
    % \phantom{\hspace{6cm}}
\end{equation}

\noindent where $\psi^{C}_m(t)$ is the yaw angle of the marker in the drone reference coordinate system and is available in the marker detection process.

\subsubsection{Cooperative strategy}
\label{subsc:cooperative_strategy}

Upon receipt of the task orders sent by the system, the two drones would perform the task without communication, where the load state estimated by each drone might differ slightly, which might interrupt the task owing to their asynchronous execution of tasks. Therefore, an algorithm for performing tasks 
(Algorithm \ref{alg:cooperative_strategy}) 
was proposed. Besides the main judgment as to whether the load has reached the target position and attitude, depending on the load states, target position, and attitude of loads, and the next target position and attitude of loads, we can discern if the accompanying drone could determine whether it should proceed to perform the task to achieve the next target position and attitude.

When performing a task, the drones moved to bring the load closer to the target position and attitude, and when the load achieved the target position and attitude with a small tolerance, the drone proceeded to perform the task to achieve the next target position and attitude. This method was feasible when a single drone performed a task, or when multiple drones performed a task with only one target. However, when multiple drones cooperated to perform a multi-target task, the task could be interrupted because of different task progress caused by errors in target positions and attitudes determined by the drones.

For example, when the two drones cooperated to carry loads, the load state of one drone had been estimated to reach the target position and attitude $p_l^d$ with an allowable tolerance, but that of the other drone had still not reached the allowable tolerance. Without communication, one drone would begin to adjust the load to achieve the next position and attitude ${p_l^d}^{'}$, while the other would not, which interrupted the task in that both parties could not enable the load to achieve the target position and attitude.

Our proposed strategy was inspired by an ordered military parade. Without communicating with each other, the soldiers still moved in an orderly manner because they were usually trained to rely on their individual marching rhythm and the state of neighboring soldiers (actions, sounds, etc.). Based on this concept, the drone was designed to determine whether its partners had achieved their targets when the target positions and attitudes of the drones were very close to their target counterparts.

In addition to the allowable tolerance of the original target position and attitude, another (larger) tolerance was set to serve as the threshold of whether the position and attitude of the load was close to its target counterpart. When the target position and attitude was determined to be approximated, the drone needed to determine whether its partners had achieved the target positon and attitude, and whether it had proceeded to perform the task to achieve the next target position and attitude. This enabled the drone to understand whether its task progress was consistent with that of its partner. With this, in the event of an estimation error, if one drone proceeded to perform the next task, the other would follow, achieving the effect of an approximately simultaneous task execution.

\begin{figure}[!t]
    \removelatexerror
    \begin{algorithm}[H]
        \SetAlgoNoEnd
        \caption{Cooperative strategy}
        \label{alg:cooperative_strategy}
        \KwIn{\\ \Indp $p_l$: Load position and attitude \\ $p_l^d$: Target load position and attitude \\ ${p_l^d}^{'}$: Next target load position and attitude \\ $isClosedflag$: Flag of load is closed to $p_l^d$ }
        \KwOut{\\ \Indp $true$ / $false$: Perform to reach ${p_l^d}^{'}$ or not}
        \If{$p_l$ reached $p_l^d$}{
            \Return{$true$}
        }
        \Else{
            \If{$p_l$ closed $p_l^d$}{
                $isClosedflag = true$
            }
            \If{$isClosedflag$}{
                \If{$p_l$ is close to ${p_l^d}^{'}$ from $p_l^d$}{
                    \Return{$true$}
                }
            }
        }
        \Return{$false$}
    \end{algorithm}
\end{figure}
