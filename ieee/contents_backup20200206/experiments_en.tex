\section{Experiment}
\label{cha:experiments}

This section describes simulation experiments and evaluations, aimed mainly at controlling the load-carrying stability and attitude, for the collaborative transportation system we designed, where we devised a scene and path allowing a load with different attitudes to pass. The experiments were conducted under different conditions and effects of external factors.

\subsection{Environment and settings}

The Gazebo\cite{gazebo} simulator was used for the experiments, where two quad drones equipped with a camera providing a bird's-eye view at the bottom, and carrying a cuboid-shaped load (as shown in Figure \ref{fig:drones_and_load}) of approximately 4 kg, were simulated in an experimental scene in the simulator.

The experimental scene comprised four parts (as shown in Figure\ref{fig:experiment_scene_region_labeled}): the first part included the ascending and descending channels, where the two drones were designed to pass through the channels in tandem, with the load kept tilted. The second part was a tilted channel, where the two drones were designed to pass through the channels in parallel, with the load kept tilted. The third part was related to how the system could rapidly and directly move the load to achieve the desired position and attitude; the last part was the curved channel, which was designed to allow the system to pass while controlling the rotation of the load.

\begin{figure} [!t]
    \begin{center}
        \includegraphics[width=70mm,trim={0 0 0 100},clip]{figures/experiment_scene_region_labeled}
        \caption{Experimental scene} 
        \label{fig:experiment_scene_region_labeled}
    \end{center}
\end{figure}

First we conducted a control group experiment in the experimental scene, in which a 6-kg cuboid load with a length of 100 cm, width of 15 cm, height of 10 cm, and density of 0.4 g/cm$^{-3}$ was suspended with a cable of length 40 cm. Next, to evaluate the stability of the system under different conditions and external factors, we performed three experiments with different settings:

\begin{itemize}
    \item \mbox{Experiment 1: Load} weight adjustment.
    \item \mbox{Experiment 2: Cable} length adjustment.
    \item \mbox{Experiment 3: Experimental} environment with wind added.
\end{itemize}

In Experiment 1, the height of the load was adjusted to 8, 6, and 4 cm, with the weight being 4.8, 3.6, and 2.4 kg, respectively, to test the stability of the system suspended with loads of different weights. In Experiment 2, the cable length was adjusted to 50, 60, 70, and 80 cm to evaluate the stability under different cable lengths. Moreover, because the adjustment of the cable length changed the size of the marker in the image, the results of this experiment could also be used to assess the stability of the load state for markers with different sizes. In Experiment 3, winds with a Beaufort scale of 4–5 (wind speed of 5.5–10.7 m/s), longer lasting breezes, winds with a Beaufort scale of 6–7 (wind speed of 10.8– 17.1 m/s), and shorter lasting breezes were added to observe the correction effect and stability of the system subject to the influence of external factors.

\subsection{Control group results}

The results of the control group (Figure \ref{fig:experiment_base}) show the change and estimation error in the position and attitude of the load, with black lines used to separate the four parts: (1) ascending and descending channels, (2) tilted channels, (3) load rapidly moved by the system, and (4) curved channels.

Figure \ref{fig:experiment_base:payload_pose_estimation_error} suggests that during the transportation process, the position and attitude of the load have a low probability of generating a large estimation error. The large estimation error is due to the erroneous marking direction (yaw, $\psi$) during the marker detection, resulting in an error in the estimated load position and attitude. The statistics of the errors in estimating the load position and attitude for all experiments, accounting for less than 1\% of all estimated errors, was found to have no impact on drone control.

\begin{figure} [!t]
    \centering
    \subfloat[Changes in position and attitude of the load]{
        \includegraphics[width=85mm]{figures/experiment_base/payload_pose}
        \label{fig:experiment_base:payload_pose}
    } \\
    \subfloat[Estimation errors in position and attitude of the load]{
        \includegraphics[width=85mm]{figures/experiment_base/payload_pose_estimation_error}
        \label{fig:experiment_base:payload_pose_estimation_error}
    }
    \caption{Control group results} 
    \label{fig:experiment_base}
\end{figure}

\subsection{Effect of load weight adjustment}
\label{subsc:experiment_1}

Three loads with different weights were applied in the control group experiments, with the experiment for each load case conducted 10 times. The results are given in Tables \ref{tbl:experiment_1:swing_angle} and \ref{tbl:experiment_1:estimation_error}. The swing angle of the load estimated from the relative location between the marker and drone was used as the basis to evaluate the load state stability of the system. Table\ref{tbl:experiment_1:swing_angle} suggests that a lighter load leads to a larger range of swing angle, with an average swing angle less than 2.5$^{\circ}$. Based on previous findings that the larger estimated errors in the load position and attitude accounted for less than 1\% of all errors, the largest 99\% of load yaw errors in the load state estimation in the 10 experiments were used in the statistics, as given in Table\ref{tbl:experiment_1:estimation_error}; this indicates that a lighter load leads to a slightly larger average swing angle. Note that the variation in pitch and yaw of the load is greater when the load swings, causing the errors in the estimated pitch and yaw to increase as the swing magnitude increases, while the estimation errors in coordinates and rollover are not affected.

\begin{table} [!t]
    \renewcommand{\arraystretch}{1.3}
    \caption{Swing angle of the load for load weight adjustment}
    \label{tbl:experiment_1:swing_angle}
    \centering
    \input{tables/experiment_1/swing_angle_en.tex}
\end{table}

\begin{table} [!t]
    \renewcommand{\arraystretch}{1.3}
    \caption{Estimation error in position and attitude of the load for load weight adjustment}
    \label{tbl:experiment_1:estimation_error}
    \centering
    \input{tables/experiment_1/estimation_error_en.tex}
\end{table}

\subsection{Effect of cable length adjustment}

Tables \ref{tbl:experiment_2:swing_angle} and \ref{tbl:experiment_2:estimation_error} list the statistical results of swing angle and estimation errors for the 10 cable length adjustment-based experiments, indicating that the estimation errors in load position and attitude are influenced by the size of the marker in the image, with the estimation error for the 80 cm case being twice that for the 40 cm case. A longer cable makes the load swing more easily, causing a larger estimation error, which affects the drone control. The PID controller of the drone could control the swing angle of the load suspended by the 80-cm cable within an average of approximately 2$^{\circ}$ by stabilizing the relative state between the marker and drone.

\begin{table} [!t]
    \renewcommand{\arraystretch}{1.3}
    \caption{Swing angle of the load for cable length adjustment}
    \label{tbl:experiment_2:swing_angle}
    \centering
    \input{tables/experiment_2/swing_angle_en.tex}
\end{table}

\begin{table} [!t]
    \renewcommand{\arraystretch}{1.3}
    \caption{Estimation error in position and attitude of the load for cable length adjustment}
    \label{tbl:experiment_2:estimation_error}
    \centering
    \input{tables/experiment_2/estimation_error_en.tex}
\end{table}

\subsection{Effect of environmental wind}

The randomly generated wind and its effect on the system are exhibited in Figures \ref{fig:experiment_3:breeze} and \ref{fig:experiment_3:gust}. Figures \ref{fig:experiment_3:breeze:wind} and \ref{fig:experiment_3:gust:wind} show the speed and direction of the wind, respectively, with the direction of the wind indicated by the vector angle on the $x$-$y$ plane in the coordinate system $I$. Figure \ref{fig:experiment_3:breeze:payload_trajectory_deviation} and \ref{fig:experiment_3:gust:payload_trajectory_deviation} displays the deviation between the load position and desired path in the experiment, which indicates that following the influence of wind, the system was capable of moving the load position back to the desired path and correcting the load state to the desired range (Figures \ref{fig:experiment_3:breeze:payload_pose_attitude} and \ref{fig:experiment_3:gust:payload_pose_attitude}). Although the position and attitude of the load were not significantly affected by the weaker and longer lasting breeze, the overall task took a relatively long time to keep the state of the load within the desired range.

\begin{figure} [!t]
    \centering
    \subfloat[Speed and direction of the wind]{
        \includegraphics[width=85mm]{figures/experiment_3/breeze/wind}
        \label{fig:experiment_3:breeze:wind}
    } \\
    \subfloat[Deviation between the load position and desired path]{
        \includegraphics[width=85mm]{figures/experiment_3/breeze/payload_trajectory_deviation_2d}
        \label{fig:experiment_3:breeze:payload_trajectory_deviation}
    } \\
    \subfloat[Attitude of load]{
        \includegraphics[width=85mm]{figures/experiment_3/breeze/payload_pose_attitude}
        \label{fig:experiment_3:breeze:payload_pose_attitude}
    }
    \caption{Effect of breeze} 
    \label{fig:experiment_3:breeze}
\end{figure}

\begin{figure} [!t]
    \centering
    \subfloat[Speed and direction of the wind]{
        \includegraphics[width=85mm]{figures/experiment_3/gust/wind}
        \label{fig:experiment_3:gust:wind}
    } \\
    \subfloat[Deviation between the load position and desired path]{
        \includegraphics[width=85mm]{figures/experiment_3/gust/payload_trajectory_deviation_2d}
        \label{fig:experiment_3:gust:payload_trajectory_deviation}
    } \\
    \subfloat[Attitude of load]{
        \includegraphics[width=85mm]{figures/experiment_3/gust/payload_pose_attitude}
        \label{fig:experiment_3:gust:payload_pose_attitude}
    }
    \caption{Effect of gusts} 
    \label{fig:experiment_3:gust}
\end{figure}

Because the load attitude was not influenced by the breeze, the estimation error in load position and attitude was not affected in case of breeze (Table \ref{tbl:experiment_3:estimation_error}). Gusts caused the load to swing (the pitch of the load in Figure \ref{fig:experiment_3:gust:payload_pose_attitude}), which was similar to the case where the swing magnitude was affected by loads with different weights (subsection \ref{subsc:experiment_1}) because the swing of the load caused a relatively large error in the estimated pitch and yaw of the load.

\begin{table} [!t]
    \renewcommand{\arraystretch}{1.3}
    \caption{Estimation error in position and attitude of the load for environmental wind}
    \label{tbl:experiment_3:estimation_error}
    \centering
    \input{tables/experiment_3/estimation_error_en.tex}
\end{table}
