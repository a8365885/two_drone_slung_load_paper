\section{Experiment}
\label{cha:experiments}

This section describes simulation experiments and evaluations, aimed mainly at controlling the load-carrying stability and attitude, for the collaborative transportation system we designed, where we devised a scene and path allowing a load with different attitudes to pass. The experiments were conducted under different conditions and effects of external factors.

\subsection{Environment and settings}

The Gazebo7\cite{gazebo} simulator was used for the experiments, the total payload of drone is 3 kg and equipped with a camera providing a bird's-eye view at the bottom. Two drone s carrying a cuboid-shaped load (as shown in Figure \ref{fig:drones_and_load}), were simulated in an experimental scene in the simulator.

To evaluate the stability of the system under different conditions and external factors, we performed three experiments with different settings:

\begin{itemize}
    \item \mbox{Experiment 1: Load} weight adjustment. 
    The cuboid load with a length of 100 cm, width of 15 cm, and density of 0.4 g/cm$^{-3}$. The height of the load was adjusted to 2, 4, 6, 10, and 10 cm, with the weight being 1.2, 2.4, 3.6, 4.8 and 6.0 kg. The load was suspended with a cable of length 40 cm. Marker dimension is the same as load width.
    
    \item \mbox{Experiment 2: Cable} length adjustment, and fixed marker dimension in the camera image. The cuboid load with a length of 100 cm, width of 15 cm, height of 10 cm, and weight of 6 kg. The cable length was adjusted to 40, 50, 60, 70, and 80 cm.
    
    \item \mbox{Experiment 3: Experimental} environment with wind added. The load is the same as Experiment 2. The length of the was 40 cm. Add gusts and long-lasting breezes to experiment at different wind speeds.
\end{itemize}

The experimental path comprised four parts with different attitudes for transportation (as shown in Figure\ref{fig:experiment_scene} and Figure \ref{fig:experiment_scene_part}): 
the first part to keep the load tilted, where the two drones one higher and one lower, ascending and descending in tandem (Figure \ref{fig:experiment_scene_part:1}).
The second part was the same as the first one to keep the load tilted, except that the two drones move in parallel (Figure \ref{fig:experiment_scene_part:2}).
The third part sets the waypoint of transportation path farther away. The position error in the controller will be larger, which will cause the system to move faster (Figure \ref{fig:experiment_scene_part:3}).
The last part was the curved movement that moves while controlling the rotation of the load (Figure \ref{fig:experiment_scene_part:4}).

\begin{figure} [!t]
    \begin{center}
        \includegraphics[width=66mm]{figures/experiment_sence_and_way_points}
        \caption{Experimental path and sence} 
        \label{fig:experiment_scene}
    \end{center}
\end{figure}

\begin{figure} [!t]
    \centering
    \subfloat[First part]{
        \includegraphics[width=1.4in]{figures/experiment_scene_part-01}
        \label{fig:experiment_scene_part:1}
    }%
    \subfloat[Second part]{
        \includegraphics[width=0.7in]{figures/experiment_scene_part-02}
        \label{fig:experiment_scene_part:2}
    }\\
    \subfloat[Third part]{
        \includegraphics[width=1.4in]{figures/experiment_scene_part-03}
        \label{fig:experiment_scene_part:3}
    }%
    \subfloat[Fourth part]{
        \includegraphics[width=0.7in]{figures/experiment_scene_part-04}
        \label{fig:experiment_scene_part:4}
    }
    \caption{Four part of experimental scene}
    \label{fig:experiment_scene_part}
\end{figure}

The change in position and attitude of the load can be seen in Figure \ref{fig:experiment_base:payload_pose_and_path}, with black lines used to separate the four parts: (1) tilted ascending and descending in tandem, (2) tilted movement in parallel, (3) rapidly movement, and (4) curved movement. The results show that our system can change the attitude of the load during transportation.

\begin{figure} [!t]
    \begin{center}
        \includegraphics[width=85mm]{figures/experiment_base/payload_pose_2}
        \caption{Change in position and attitude of the load}
        \label{fig:experiment_base:payload_pose_and_path}
    \end{center}
\end{figure}

\subsection{Effect of load weight adjustment}
\label{subsc:experiment_1}

Five loads with different weights were conducted 10 times. The average of results are given in Table \ref{tbl:experiment_1:results}.The swing angle of the load estimated from the relative location between the marker and drone was used as the basis to evaluate the load state stability of the system. Although the lighter load leads to a larger range of swing angle, the average of swing angle on 20\% of total payload (1.2 kg) approximately 3.3 degrees, and the average of deviation between the load position and desired path was maintained at about 3.6 cm.

\begin{table} [!t]
    \renewcommand{\arraystretch}{1.3}
    \caption{Average of results in load weight adjustment}
    \label{tbl:experiment_1:results}
    \centering
    \input{tables/experiment_1/results_en.tex}
\end{table}

\subsection{Effect of cable length adjustment}

Table \ref{tbl:experiment_2:results} list the statistical results of swing angle and deviation for the 10 cable length adjustment-based experiments. Though the swing angle was affected by the length of the cable, the average of swing angle of the load is controlled less than 2 degrees, and the average deviation between the load position and desired path was less than 4.4 cm.

\begin{table} [!t]
    \renewcommand{\arraystretch}{1.3}
    \caption{Average of results in cable length adjustment}
    \label{tbl:experiment_2:results}
    \centering
    \input{tables/experiment_2/results_en.tex}
\end{table}

\subsection{Effect of environmental wind}

The randomly generated wind and its effect on the system are exhibited in Figures \ref{fig:experiment_3:gust} and \ref{fig:experiment_3:breeze}. 
Figures \ref{fig:experiment_3:gust:wind} and \ref{fig:experiment_3:breeze:wind} show the speed and direction of the wind, respectively, with the direction of the wind indicated by the vector angle on the $x$-$y$ plane in the coordinate system $I$. 
Figure \ref{fig:experiment_3:gust:payload_trajectory_deviation} and \ref{fig:experiment_3:breeze:payload_trajectory_deviation} displays the deviation between the load position and desired path in the experiment, which indicates that following the influence of wind, the system was capable of moving the load position back to the desired path and correcting the load state to the desired range (Figures \ref{fig:experiment_3:breeze:payload_pose_attitude} and \ref{fig:experiment_3:gust:payload_pose_attitude}). 

The thrust and mass of the two drones in the simulator, as well as the mass of the load, were subjected to a long-lasting breeze with more than 7 meters per second, causing the system to take a longer time to maintain the load. 
The mission failed due to long-lasting breeze exceeding 11 meters per second and gusts exceeding 15 meters per second.
The system can quickly correct the load status in the environment with gusts below 15 meters per second or long-lasting breeze below 7 meters per second, without affecting the task time (Figure \ref{fig:experiment_3:spend_time}).

\begin{figure} [!t]
    \centering
    \subfloat[Speed and direction of the wind]{
        \includegraphics[width=85mm]{figures/experiment_3/gust/wind}
        \label{fig:experiment_3:gust:wind}
    } \\
    \subfloat[Attitude of load]{
        \includegraphics[width=85mm]{figures/experiment_3/gust/payload_pose_attitude}
        \label{fig:experiment_3:gust:payload_pose_attitude}
    } \\
    \subfloat[Deviation between the load position and desired path]{
        \includegraphics[width=85mm]{figures/experiment_3/gust/payload_trajectory_deviation_2d}
        \label{fig:experiment_3:gust:payload_trajectory_deviation}
    } \\
    % \subfloat[Swing angle of the load]{
    %     \includegraphics[width=85mm]{figures/experiment_3/gust/swing_angle}
    %     \label{fig:experiment_3:gust:swing_angle}
    % }
    \caption{Effect of gusts} 
    \label{fig:experiment_3:gust}
\end{figure}

\begin{figure} [!t]
    \centering
    \subfloat[Speed and direction of the wind]{
        \includegraphics[width=85mm]{figures/experiment_3/breeze/wind}
        \label{fig:experiment_3:breeze:wind}
    } \\
    \subfloat[Attitude of load]{
        \includegraphics[width=85mm]{figures/experiment_3/breeze/payload_pose_attitude}
        \label{fig:experiment_3:breeze:payload_pose_attitude}
    } \\
    \subfloat[Deviation between the load position and desired path]{
        \includegraphics[width=85mm]{figures/experiment_3/breeze/payload_trajectory_deviation_2d}
        \label{fig:experiment_3:breeze:payload_trajectory_deviation}
    } \\
    % \subfloat[Swing angle of the load]{
    %     \includegraphics[width=85mm]{figures/experiment_3/breeze/swing_angle}
    %     \label{fig:experiment_3:breeze:swing_angle}
    % }
    \caption{Effect of breeze} 
    \label{fig:experiment_3:breeze}
\end{figure}

\begin{figure} [htp!]
    \begin{center}
        \includegraphics[width=85mm,trim={4 4 4 4},clip]{figures/experiment_3_spend_time_en}
        \caption{Average of spend time affected by environmental wind} 
        \label{fig:experiment_3:spend_time}
    \end{center}
\end{figure}
