ids:
         & 100      & 221      & 222      & 223      & 224     

payloadpose count:
         & 69270    & 66484    & 66706    & 71456    & 71494   

payloadpose truth count:
         & 69270    & 66484    & 66706    & 71456    & 71494   

yaw error >= 90 count:
         & 199      & 143      & 112      & 94       & 103     

yaw error >= 90 ratio:
         & 0.002873 & 0.002151 & 0.001679 & 0.001315 & 0.001441

all estimation error mean:
x        &   0.0069 &   0.0083 &   0.0102 &   0.0118 &   0.0137
y        &   0.0089 &   0.0098 &   0.0116 &   0.0140 &   0.0158
z        &   0.0090 &   0.0092 &   0.0096 &   0.0104 &   0.0123
$\phi$   &   0.9131 &   0.9313 &   0.9368 &   0.9285 &   0.9459
$\theta$ &   0.9927 &   0.9898 &   0.9801 &   0.9734 &   0.9867
$\psi$   &   0.7083 &   0.5839 &   0.5048 &   0.4585 &   0.4506

all estimation error max:
x        &   0.9309 &   0.8447 &   0.9526 &   0.9197 &   1.0528
y        &   0.9786 &   0.9711 &   0.8896 &   0.9577 &   1.1778
z        &   0.8149 &   1.0097 &   1.2063 &   1.3979 &   1.6176
$\phi$   &  13.7308 &   8.6339 &  14.2596 &  13.0181 &  13.7090
$\theta$ &  11.6820 &   8.4947 &  14.0135 &  14.4173 &  15.8526
$\psi$   & 181.0010 & 180.5820 & 180.9061 & 181.3906 & 180.7042

99% estimation error mean:
x        &   0.0064 &   0.0079 &   0.0098 &   0.0115 &   0.0133
y        &   0.0068 &   0.0084 &   0.0106 &   0.0132 &   0.0147
z        &   0.0069 &   0.0072 &   0.0077 &   0.0086 &   0.0101
$\phi$   &   0.9046 &   0.9257 &   0.9312 &   0.9237 &   0.9404
$\theta$ &   0.9804 &   0.9800 &   0.9712 &   0.9663 &   0.9781
$\psi$   &   0.1823 &   0.1878 &   0.1937 &   0.2095 &   0.1834

99% estimation error max:
x        &   0.0397 &   0.1029 &   0.1766 &   0.1833 &   0.1560
y        &   0.0559 &   0.6014 &   0.6441 &   0.6854 &   0.8062
z        &   0.1069 &   0.5672 &   0.5794 &   0.4863 &   0.6168
$\phi$   &  12.3501 &   8.6339 &  11.3494 &  10.1115 &   7.1240
$\theta$ &  11.6820 &   6.1065 &   8.4850 &   9.2557 &   7.7607
$\psi$   &   1.0710 &   1.0553 &   1.0008 &   1.0320 &   0.8798

payload pitch:
mean     &   1.9509 &   2.0886 &   2.0640 &   2.2118 &   2.1954
max      &  15.8492 &  15.8230 &  10.9717 &  14.1499 &  10.6505

swing angle:
mean     &   1.7464 &   1.8108 &   1.8535 &   1.9951 &   1.9494
max      &  22.3369 &  15.6603 &  11.0489 &  20.7410 &  11.4029

swing angle 2:
mean     &   3.2926 &   3.3339 &   3.4228 &   3.3759 &   3.4724
max      &  30.2940 &  26.9120 &  28.3123 &  25.2208 &  25.6038

spend time:
mean     & 115.4170 & 110.7740 & 111.1430 & 119.0610 & 119.1240
max      & 128.9000 & 119.6600 & 117.1700 & 129.6300 & 127.1300

trajectory deviation:
mean     &   0.0557 &   0.0564 &   0.0553 &   0.0596 &   0.0690
max      &   0.2953 &   0.2446 &   0.3086 &   0.3386 &   0.3129

trajectory deviation 2d:
mean     &   0.0362 &   0.0387 &   0.0392 &   0.0412 &   0.0439
max      &   0.2950 &   0.2432 &   0.3086 &   0.3386 &   0.3113

