ids:
         & 100      & 121      & 122      & 123      & 124     

payloadpose count:
         & 69270    & 68072    & 69798    & 72178    & 76432   

payloadpose truth count:
         & 69270    & 68072    & 69798    & 72178    & 76432   

yaw error >= 90 count:
         & 199      & 270      & 395      & 726      & 827     

yaw error >= 90 ratio:
         & 0.002873 & 0.003966 & 0.005659 & 0.010058 & 0.010820

all estimation error mean:
x        &   0.0069 &   0.0090 &   0.0111 &   0.0139 &   0.0160
y        &   0.0089 &   0.0110 &   0.0141 &   0.0192 &   0.0239
z        &   0.0090 &   0.0126 &   0.0179 &   0.0288 &   0.0363
$\phi$   &   0.9131 &   1.1066 &   1.3328 &   1.6456 &   1.8338
$\theta$ &   0.9927 &   1.2036 &   1.5177 &   1.7894 &   2.2250
$\psi$   &   0.7083 &   0.9430 &   1.3151 &   2.1599 &   2.3603

all estimation error max:
x        &   0.9309 &   0.9444 &   0.9579 &   1.0273 &   1.0256
y        &   0.9786 &   0.9623 &   1.1158 &   1.1152 &   1.1711
z        &   0.8149 &   1.0170 &   1.2410 &   1.4548 &   1.9580
$\phi$   &  13.7308 &  13.1262 & 176.8028 &  24.2960 & 184.2149
$\theta$ &  11.6820 &  10.8308 &  19.8973 &  21.4745 &  33.7308
$\psi$   & 181.0010 & 181.0315 & 180.8942 & 181.8647 & 182.2400

99% estimation error mean:
x        &   0.0064 &   0.0080 &   0.0097 &   0.0116 &   0.0135
y        &   0.0068 &   0.0084 &   0.0101 &   0.0123 &   0.0165
z        &   0.0069 &   0.0089 &   0.0115 &   0.0153 &   0.0197
$\phi$   &   0.9046 &   1.0997 &   1.3180 &   1.6211 &   1.7879
$\theta$ &   0.9804 &   1.1892 &   1.4963 &   1.7698 &   2.1971
$\psi$   &   0.1823 &   0.2223 &   0.2897 &   0.3529 &   0.4191

99% estimation error max:
x        &   0.0397 &   0.0615 &   0.1051 &   0.1730 &   0.7467
y        &   0.0559 &   0.6298 &   0.7065 &   0.7497 &   0.8361
z        &   0.1069 &   0.6413 &   0.6265 &   0.6419 &   0.8953
$\phi$   &  12.3501 &  11.7277 &  17.3858 &  24.2960 & 184.2149
$\theta$ &  11.6820 &  10.8308 &  19.8973 &  21.4745 &  33.7308
$\psi$   &   1.0710 &   1.2378 &   1.6796 &   4.4599 &  85.5351

payload pitch:
mean     &   1.9509 &   1.9615 &   2.3119 &   2.2536 &   2.5372
max      &  15.8492 &  12.5226 &  14.3128 &  12.1154 &  15.1181

swing angle:
mean     &   1.7464 &   1.7452 &   1.9244 &   1.9840 &   2.0016
max      &  22.3369 &  15.0938 &  15.3848 &  14.0124 &  14.3923

swing angle 2:
mean     &   3.2926 &   3.2840 &   3.5486 &   3.6101 &   3.7103
max      &  30.2940 &  27.9089 &  25.1016 &  22.2681 &  26.8634

spend time:
mean     & 115.4170 & 113.4190 & 116.2980 & 120.2630 & 127.3520
max      & 128.9000 & 119.1300 & 124.5700 & 131.2400 & 141.2000

trajectory deviation:
mean     &   0.0557 &   0.0589 &   0.0668 &   0.0790 &   0.0854
max      &   0.2953 &   0.2723 &   0.3337 &   0.2828 &   0.3674

trajectory deviation 2d:
mean     &   0.0362 &   0.0383 &   0.0436 &   0.0459 &   0.0469
max      &   0.2950 &   0.2721 &   0.3309 &   0.2795 &   0.3650

