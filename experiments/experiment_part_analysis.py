#!/usr/bin/env python

import argparse
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import json
import math
import draw_chart_experiment

COLOR_BLUE = 'b'
COLOR_GREEN = 'g'
COLOR_RED = 'r'
COLOR_CYAN = 'c'
COLOR_MAGENTA = 'm'
COLOR_YELLOW = 'y'
COLOR_BLACK = 'k'
COLOR_WHITE = 'w'
COLORS = [COLOR_BLUE, COLOR_GREEN, COLOR_RED, COLOR_CYAN, COLOR_MAGENTA, COLOR_YELLOW, COLOR_BLACK, COLOR_WHITE]

def GetDroneEstimationPayloadPose(droneFolder, startTime=15., endTime=35., step=0.1):
    filePath = droneFolder + '/payloadpose.txt'
    filePath_truth = droneFolder + '/payloadpose_truth.txt'
    payloadpose = draw_chart_experiment.GetFileData(filePath, startTime, endTime, step)
    payloadpose_truth = draw_chart_experiment.GetFileData(filePath_truth, startTime, endTime, step)
    return [payloadpose, payloadpose_truth]

def GetDronePose(droneFolder, startTime=15., endTime=35., step=0.1):
    filePath = droneFolder + '/dronepose.txt'
    dronepose = draw_chart_experiment.GetFileData(filePath, startTime, endTime, step)
    return dronepose

def GetMarkerPose(droneFolder, startTime=15., endTime=35., step=0.1):
    filePath = droneFolder + '/markerpose_truth.txt'
    markerpose_truth = draw_chart_experiment.GetFileData(filePath, startTime, endTime, step)
    return markerpose_truth

def FilterMaxYawEstimationError(payloadpose, payloadpose_truth, percentage=1.0):
    times = [row[0] for row in payloadpose]
    datas = []
    for t in range(len(times)):
        data = []
        data.extend(payloadpose[t])
        data.extend(payloadpose_truth[t])
        data.append(abs(payloadpose[t][6] - payloadpose_truth[t][6]))
        datas.append(data)
    datas.sort(key=lambda x:abs(x[14]))
    newLen = int(len(datas) * percentage)
    datas = datas[0:newLen]
    return [[row[0:7] for row in datas], [row[7:14] for row in datas]]

def CalcDroneEstimationError(payloadpose, payloadpose_truth):
    allErrors = []
    times = [row[0] for row in payloadpose]
    for t in range(len(times)):
        errors = [times[t]]
        for i in range(6):
            error = payloadpose[t][i + 1] - payloadpose_truth[t][i + 1]
            errors.append(error)
        allErrors.append(errors)

    meanErrors = []
    maxErrors = []
    for i in range(6):
        data = [abs(n) for n in [row[i + 1] for row in allErrors]]
        data.sort(reverse=True)

        sumError = sum(data)
        meanError = sumError / len(data)
        maxError = data[0]

        meanErrors.append(meanError)
        maxErrors.append(maxError)
    return meanErrors, maxErrors

def CalcPayloadPitch(payloadpose_truth):
    data = [abs(n) for n in [row[5] for row in payloadpose_truth]]
    data.sort(reverse=True)

    sumPayloadPitch = sum(data)
    meanPayloadPitch = sumPayloadPitch / len(data)
    maxPayloadPitch = data[0]

    return meanPayloadPitch, maxPayloadPitch

def CalcSwingAngle(dronepose, markerpose, cablelength):
    allSwingAngle = []
    times = [row[0] for row in dronepose]
    for t in range(len(times)):
        dx = dronepose[t][1]
        mx = markerpose[t][1]
        l = dx - mx
        angle = math.atan2(l, cablelength) * 180 / math.pi

        swingAngle = [times[t], angle]
        allSwingAngle.append(swingAngle)

    data = [abs(n) for n in [row[1] for row in allSwingAngle]]
    data.sort(reverse=True)

    sumSwingAngle = sum(data)
    meanSwingAngle = sumSwingAngle / len(data)
    maxSwingAngle = data[0]

    return meanSwingAngle, maxSwingAngle

def CalcSwingAngle2(dronepose, markerpose, cablelength):
    allSwingAngle = []
    times = [row[0] for row in dronepose]
    for t in range(len(times)):
        dx = dronepose[t][1]
        dy = dronepose[t][2]
        mx = markerpose[t][1]
        my = markerpose[t][2]
        l = math.sqrt((dx - mx) * (dx - mx) + (dy - my) * (dy - my))
        angle = math.atan2(l, cablelength) * 180 / math.pi

        swingAngle = [times[t], angle]
        allSwingAngle.append(swingAngle)

    data = [abs(n) for n in [row[1] for row in allSwingAngle]]
    data.sort(reverse=True)

    sumSwingAngle = sum(data)
    meanSwingAngle = sumSwingAngle / len(data)
    maxSwingAngle = data[0]

    return meanSwingAngle, maxSwingAngle

if __name__ == '__main__':
    argParser = argparse.ArgumentParser()
    argParser.add_argument('--folder', type=str, default='.')
    argParser.add_argument('--timeStep', type=float, default=0.0)
    argParser.add_argument('--ids', type=str, nargs='+')
    argParser.add_argument('--cablelength', '-cl', type=float, default=0.4)
    argParser.add_argument('--cablelengths', '-cls', type=float, nargs='+')
    args = argParser.parse_args()

    for part in range(4):
        print("####################################################################################################")
        print("Part: " + str(part + 1))

        all_payloadpose = []
        all_payloadpose_truth = []
        all_dronepose = []
        all_markerpose = []
        for id in args.ids:
            experiment_payloadpose = []
            experiment_payloadpose_truth = []
            experiment_dronepose = []
            experiment_markerpose = []
            for subid in range(1, 11):
                # print('subid: ' + str(subid))
                experiment_folder = args.folder + '/research_experiment_' + id + '_' + str(subid)
                config = draw_chart_experiment.GetConfig(experiment_folder)
                starttime = config['timestamp'][part]
                endtime = config['timestamp'][part + 1]
                timestep = args.timeStep
                payloadpose, payloadpose_truth = GetDroneEstimationPayloadPose(experiment_folder + '/ardrone_1', startTime=starttime, endTime=endtime, step=timestep)
                experiment_payloadpose.extend(payloadpose)
                experiment_payloadpose_truth.extend(payloadpose_truth)
                # print('payloadpose count: ' + str(len(payloadpose)))
                payloadpose, payloadpose_truth = GetDroneEstimationPayloadPose(experiment_folder + '/ardrone_2', startTime=starttime, endTime=endtime, step=timestep)
                experiment_payloadpose.extend(payloadpose)
                experiment_payloadpose_truth.extend(payloadpose_truth)
                # print('payloadpose count: ' + str(len(payloadpose)))
                dronepose = GetDronePose(experiment_folder + '/ardrone_1', startTime=starttime, endTime=endtime, step=timestep)
                markerpose = GetMarkerPose(experiment_folder + '/ardrone_1', startTime=starttime, endTime=endtime, step=timestep)
                experiment_dronepose.extend(dronepose)
                experiment_markerpose.extend(markerpose)
                dronepose = GetDronePose(experiment_folder + '/ardrone_2', startTime=starttime, endTime=endtime, step=timestep)
                markerpose = GetMarkerPose(experiment_folder + '/ardrone_2', startTime=starttime, endTime=endtime, step=timestep)
                experiment_dronepose.extend(dronepose)
                experiment_markerpose.extend(markerpose)
            all_payloadpose.append(experiment_payloadpose)
            all_payloadpose_truth.append(experiment_payloadpose_truth)
            all_dronepose.append(experiment_dronepose)
            all_markerpose.append(experiment_markerpose)

        str_ids = "{:<8}".format("")
        str_payloadpose_count = "{:<8}".format("")
        str_payloadpose_truth_count = "{:<8}".format("")
        str_yaw_error_90_count = "{:<8}".format("")
        str_yaw_error_90_ratio = "{:<8}".format("")
        str_estimation_error_100_mean = ["{:<8}".format("x"), "{:<8}".format("y"), "{:<8}".format("z"), "{:<8}".format("$\phi$"), "{:<8}".format("$\\theta$"), "{:<8}".format("$\psi$")]
        str_estimation_error_100_max = ["{:<8}".format("x"), "{:<8}".format("y"), "{:<8}".format("z"), "{:<8}".format("$\phi$"), "{:<8}".format("$\\theta$"), "{:<8}".format("$\psi$")]
        str_estimation_error_99_mean = ["{:<8}".format("x"), "{:<8}".format("y"), "{:<8}".format("z"), "{:<8}".format("$\phi$"), "{:<8}".format("$\\theta$"), "{:<8}".format("$\psi$")]
        str_estimation_error_99_max = ["{:<8}".format("x"), "{:<8}".format("y"), "{:<8}".format("z"), "{:<8}".format("$\phi$"), "{:<8}".format("$\\theta$"), "{:<8}".format("$\psi$")]
        str_payload_pitch_mean = "{:<8}".format("mean")
        str_payload_pitch_max = "{:<8}".format("max")
        str_swing_angle_mean = "{:<8}".format("mean")
        str_swing_angle_max = "{:<8}".format("max")
        str_swing_angle_2_mean = "{:<8}".format("mean")
        str_swing_angle_2_max = "{:<8}".format("max")

        index = 0
        for id in args.ids:
            str_ids += " & " + "{:<8}".format(id)
            str_payloadpose_count += " & " + "{:<8d}".format(len(all_payloadpose[index]))
            str_payloadpose_truth_count += " & " + "{:<8d}".format(len(all_payloadpose_truth[index]))

            yaw_error_90_count = 0
            yaw_error_90_ratio = 0
            for t in range(len(all_payloadpose[index])):
                yaw_error = all_payloadpose[index][t][6] - all_payloadpose_truth[index][t][6]
                if abs(yaw_error) >= 90:
                    yaw_error_90_count += 1
            yaw_error_90_ratio = float(yaw_error_90_count) / float(len(all_payloadpose[index]))
            str_yaw_error_90_count += " & " + "{:<8d}".format(yaw_error_90_count)
            str_yaw_error_90_ratio += " & " + "{:0.6f}".format(yaw_error_90_ratio)
        
            filter100_payloadpose, filter100_payloadpose_truth = FilterMaxYawEstimationError(all_payloadpose[index], all_payloadpose_truth[index], percentage=1.0)
            meanErrors, maxErrors = CalcDroneEstimationError(filter100_payloadpose, filter100_payloadpose_truth)
            for i in range(6):
                str_estimation_error_100_mean[i] += " & " + "{:8.4f}".format(meanErrors[i])
                str_estimation_error_100_max[i] += " & " + "{:8.4f}".format(maxErrors[i])
        
            filter99_payloadpose, filter99_payloadpose_truth = FilterMaxYawEstimationError(all_payloadpose[index], all_payloadpose_truth[index], percentage=0.99 if yaw_error_90_ratio <= 0.01 else 1.0 - yaw_error_90_ratio)
            meanErrors, maxErrors = CalcDroneEstimationError(filter99_payloadpose, filter99_payloadpose_truth)
            for i in range(6):
                str_estimation_error_99_mean[i] += " & " + "{:8.4f}".format(meanErrors[i])
                str_estimation_error_99_max[i] += " & " + "{:8.4f}".format(maxErrors[i])
            
            meanPayloadPitch, maxPayloadPitch = CalcPayloadPitch(all_payloadpose_truth[index])
            str_payload_pitch_mean += " & " + "{:8.4f}".format(meanPayloadPitch)
            str_payload_pitch_max += " & " + "{:8.4f}".format(maxPayloadPitch)

            cablelength = args.cablelengths[index] if args.cablelengths and len(args.cablelengths) == len(args.ids) else args.cablelength

            meanSwingAngle, maxSwingAngle = CalcSwingAngle(all_dronepose[index], all_markerpose[index], cablelength)
            str_swing_angle_mean += " & " + "{:8.4f}".format(meanSwingAngle)
            str_swing_angle_max += " & " + "{:8.4f}".format(maxSwingAngle)

            meanSwingAngle, maxSwingAngle = CalcSwingAngle2(all_dronepose[index], all_markerpose[index], cablelength)
            str_swing_angle_2_mean += " & " + "{:8.4f}".format(meanSwingAngle)
            str_swing_angle_2_max += " & " + "{:8.4f}".format(maxSwingAngle)

            index += 1

        print('')
        print("ids:\n" + str_ids)
        print('')
        print("payloadpose count:\n" + str_payloadpose_count)
        print('')
        print("payloadpose truth count:\n" + str_payloadpose_truth_count)
        print('')
        print("yaw error >= 90 count:\n" + str_yaw_error_90_count)
        print('')
        print("yaw error >= 90 ratio:\n" + str_yaw_error_90_ratio)
        print('')
        print("all estimation error mean:")
        for i in range(6):
            print(str_estimation_error_100_mean[i])
        print('')
        print("all estimation error max:")
        for i in range(6):
            print(str_estimation_error_100_max[i])
        print('')
        print("99% estimation error mean:")
        for i in range(6):
            print(str_estimation_error_99_mean[i])
        print('')
        print("99% estimation error max:")
        for i in range(6):
            print(str_estimation_error_99_max[i])
        print('')
        print("payload pitch:")
        print(str_payload_pitch_mean)
        print(str_payload_pitch_max)
        print('')
        print("swing angle:")
        print(str_swing_angle_mean)
        print(str_swing_angle_max)
        print('')
        print("swing angle 2:")
        print(str_swing_angle_2_mean)
        print(str_swing_angle_2_max)
        print('')

        print("####################################################################################################")
    