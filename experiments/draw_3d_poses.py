#!/usr/bin/env python

import argparse
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from itertools import product, combinations
import math

COLOR_BLUE = 'b'
COLOR_GREEN = 'g'
COLOR_RED = 'r'
COLOR_CYAN = 'c'
COLOR_MAGENTA = 'm'
COLOR_YELLOW = 'y'
COLOR_BLACK = 'k'
COLOR_WHITE = 'w'
COLORS = [COLOR_BLUE, COLOR_GREEN, COLOR_RED, COLOR_CYAN, COLOR_MAGENTA, COLOR_YELLOW, COLOR_BLACK, COLOR_WHITE]

def Draw3DPoints(filePath, plot, color=None, startTime=10., step=2.):
    xs = []
    ys = []
    zs = []
    stepTime = 0

    print(filePath)
    with open(filePath, 'r') as f:
        lines = f.readlines()
        for line in lines:
            valuesStr = line.split('\t')
            time = float(valuesStr[0])
            if time > startTime + stepTime:
                xs.append(float(valuesStr[1]))
                ys.append(float(valuesStr[2]))
                zs.append(float(valuesStr[3]))
                stepTime += step

    plot.scatter(np.array(xs), np.array(ys), zs=np.array(zs), c=color, label=filePath)
    return None

def Draw3DLine(filePath, plot, color=None, startTime=10., step=2.):
    xs = []
    ys = []
    zs = []
    stepTime = 0

    print(filePath)
    with open(filePath, 'r') as f:
        lines = f.readlines()
        for line in lines:
            valuesStr = line.split('\t')
            time = float(valuesStr[0])
            if time > startTime + stepTime:
                xs.append(float(valuesStr[1]))
                ys.append(float(valuesStr[2]))
                zs.append(float(valuesStr[3]))
                stepTime += step

    plot.plot(np.array(xs), np.array(ys), zs=np.array(zs), c=color, label=filePath)
    return None

def Draw3DCube(plot, w, l, h, x, y, z, roll, pitch, yaw, color, label=None):
    dw = [-w / 2., w / 2.]
    dl = [-l / 2., l / 2.]
    dh = [-h / 2., h / 2.]
    for s, e in combinations(np.array(list(product(dw,dl,dh))), 2):
        sameCount = 0
        for i in range(3):
            if s[i] == e[i]:
                sameCount += 1
        if sameCount == 2:
            s = [s[0], s[1] * math.cos(np.radians(roll)) - s[2] * math.sin(np.radians(roll)), s[1] * math.sin(np.radians(roll)) + s[2] * math.cos(np.radians(roll))]
            e = [e[0], e[1] * math.cos(np.radians(roll)) - e[2] * math.sin(np.radians(roll)), e[1] * math.sin(np.radians(roll)) + e[2] * math.cos(np.radians(roll))]
            s = [s[2] * math.sin(np.radians(roll)) + s[0] * math.cos(np.radians(roll)), s[1], s[2] * math.cos(np.radians(roll)) - s[0] * math.sin(np.radians(roll))]
            e = [e[2] * math.sin(np.radians(roll)) + e[0] * math.cos(np.radians(roll)), e[1], e[2] * math.cos(np.radians(roll)) - e[0] * math.sin(np.radians(roll))]
            s = [s[0] * math.cos(np.radians(yaw)) - s[1] * math.sin(np.radians(yaw)), s[0] * math.sin(np.radians(yaw)) + s[1] * math.cos(np.radians(yaw)), s[2]]
            e = [e[0] * math.cos(np.radians(yaw)) - e[1] * math.sin(np.radians(yaw)), e[0] * math.sin(np.radians(yaw)) + e[1] * math.cos(np.radians(yaw)), e[2]]
            plot.plot(np.array([s[0] + x, e[0] + x]), np.array([s[1] + y, e[1] + y]), zs=np.array([s[2] + z, e[2] + z]), c=color, label=label)

def Draw3DPoses(filePath, plot, w=0.1, l=0.1, h=0.1, color=None, startTime=10., step=2.):
    stepTime = 0

    print(filePath)
    with open(filePath, 'r') as f:
        lines = f.readlines()
        for line in lines:
            valuesStr = line.split('\t')
            time = float(valuesStr[0])
            if time > startTime + stepTime:
                Draw3DCube(plot, w, l, h, float(valuesStr[1]), float(valuesStr[2]), float(valuesStr[3]), float(valuesStr[4]), float(valuesStr[5]), float(valuesStr[6]), color)
                stepTime += step

def Init3DPlot(fig):
    return Axes3D(fig)

if __name__ == '__main__':
    argParser = argparse.ArgumentParser()
    argParser.add_argument('--files', type=str, nargs='+')
    args = argParser.parse_args()

    fig = plt.figure()
    plot = Init3DPlot(fig)
    plot.set_xlim3d(-10, 10)
    plot.set_ylim3d(-10, 10)
    plot.set_zlim3d(-10, 10)

    count = 0
    for filePath in args.files:
        Draw3DPoses(filePath, plot, w=0.15, l=1.0, h=0.15, color=COLORS[count % len(COLORS)], startTime=10., step=2.)
        count += 1


    plt.show()
