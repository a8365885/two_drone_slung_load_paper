#!/usr/bin/env python

import argparse
import numpy as np
import matplotlib.pyplot as plt
import json
from mpl_toolkits.mplot3d import Axes3D
from itertools import product, combinations
import math

COLOR_BLUE = 'b'
COLOR_GREEN = 'g'
COLOR_RED = 'r'
COLOR_CYAN = 'c'
COLOR_MAGENTA = 'm'
COLOR_YELLOW = 'y'
COLOR_BLACK = 'k'
COLOR_WHITE = 'w'
COLORS = [COLOR_BLUE, COLOR_GREEN, COLOR_RED, COLOR_CYAN, COLOR_MAGENTA, COLOR_YELLOW, COLOR_BLACK, COLOR_WHITE]

def GetFileData(filePath, startTime=15., endTime=35., step=0.1):
    data = []

    stepTime = 0
    with open(filePath, 'r') as f:
        lines = f.readlines()
        for line in lines:
            valuesStr = line.split('\t')
            time = float(valuesStr[0])
            if (time >= startTime + stepTime) & (time <= endTime):
                data.append([
                    float(valuesStr[0]) - startTime, 
                    float(valuesStr[1]), 
                    float(valuesStr[2]), 
                    float(valuesStr[3]), 
                    float(valuesStr[4]), 
                    float(valuesStr[5]), 
                    float(valuesStr[6])])
                stepTime += step

    return data

def DrawLineChart(filePath, dataCol, dtatName, startTime=15., endTime=35., step=0.1):
    data = GetFileData(filePath, startTime, endTime, step)
    x = [row[0] for row in data]
    y = [row[dataCol] for row in data]
    plt.plot(x, y, '-', label=dtatName)


def DrawLineChart2(filePath, dataCol1, dataCol2, dtatName, startTime=15., endTime=35., step=0.1):
    data = GetFileData(filePath, startTime, endTime, step)
    x = [row[dataCol1] for row in data]
    y = [row[dataCol2] for row in data]
    plt.plot(x, y, '-', label=dtatName)


def DrawTrajectory(filePath, dataCol1, dataCol2, dtatName):
    x = []
    y = []

    with open(filePath, 'r') as f:
        lines = f.readlines()
        for line in lines:
            valuesStr = line.split('\t')
            x.append(float(valuesStr[dataCol1]))
            y.append(float(valuesStr[dataCol2]))

    plt.plot(x, y, '.-', label=dtatName)

def Draw3DPosition(filePath, plot, color=None, startTime=15., endTime=35., step=0.1):
    data = GetFileData(filePath, startTime, endTime, step)
    x = [row[1] for row in data]
    y = [row[2] for row in data]
    z = [row[3] for row in data]
    plot.plot(np.array(x), np.array(y), zs=np.array(z), c=color, label=filePath)
    return None

def Draw3DTrajectory(filePath, plot, color=None):
    x = []
    y = []
    z = []

    with open(filePath, 'r') as f:
        lines = f.readlines()
        for line in lines:
            valuesStr = line.split('\t')
            x.append(float(valuesStr[0]))
            y.append(float(valuesStr[1]))
            z.append(float(valuesStr[2]))

    plot.plot(np.array(x), np.array(y), zs=np.array(z), c=color, label=filePath)
    return None

def DroneEstimationError(folder, startTime=15., endTime=35., step=0.1):
    filePaths = [folder + '/payloadpose.txt', folder + '/payloadpose_truth.txt']
    datas = []

    for filePath in filePaths:
        data = GetFileData(filePath, startTime, endTime, step)
        datas.append(data)

    sumErrors = []
    maxErrors = []
    times = [[row[0] for row in datas[0]], [row[0] for row in datas[1]]]
    for i in range(6):
        sumError = 0
        maxError = 0
        for j in range(len(times[0])):
            error = abs(datas[0][j][i+1] - datas[1][j][i+1])
            sumError += error
            if error > maxError:
                maxError = error
        sumErrors.append(sumError)
        maxErrors.append(maxError)
    meanErrors = [item / len(times[0]) for item in sumErrors]
    return meanErrors, maxErrors

def DrawDroneEstimationError(folder, dataCol, dtatName, startTime=15., endTime=35., step=0.1):
    filePaths = [folder + '/payloadpose.txt', folder + '/payloadpose_truth.txt']
    datas = []

    for filePath in filePaths:
        data = GetFileData(filePath, startTime, endTime, step)
        datas.append(data)

    times = [[row[0] for row in datas[0]], [row[0] for row in datas[1]]]
    errors = []
    for j in range(len(times[0])):
        errors.append(datas[0][j][dataCol] - datas[1][j][dataCol])
    plt.plot(times[0], errors, '-', label=dtatName)

if __name__ == '__main__':
    argParser = argparse.ArgumentParser()
    argParser.add_argument('--folder', type=str)
    args = argParser.parse_args()

    config = {
        'startTime': 0.0, 
        'endTime': 10.0, 
        'timeStep': 0.1,
        'xmin': -10,
        'xmax': 10,
        'ymin': -10,
        'ymax': 10,
        "zmin": -10, 
        "zmax": 10,
        "mainAxis": 0
        }

    try:
        with open(args.folder + '/config.json', 'r') as f:
            config = json.load(f)
    except IOError:
        with open(args.folder + '/config.json', 'w') as f: 
            json.dump(config, f, indent=4)

    fig = plt.figure(figsize=(10, 12))

    plt.subplot(6, 1, 1)
    DrawLineChart(args.folder + '/ardrone_1/payloadpose.txt', 1, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawLineChart(args.folder + '/ardrone_2/payloadpose.txt', 1, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawLineChart(args.folder + '/ardrone_1/payloadpose_truth.txt', 1, 'ground truth', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    # plt.legend(loc='upper right')
    plt.ylabel('x')

    plt.subplot(6, 1, 2)
    DrawLineChart(args.folder + '/ardrone_1/payloadpose.txt', 2, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawLineChart(args.folder + '/ardrone_2/payloadpose.txt', 2, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawLineChart(args.folder + '/ardrone_1/payloadpose_truth.txt', 2, 'ground truth', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    # plt.legend(loc='upper right')
    plt.ylabel('y')

    plt.subplot(6, 1, 3)
    DrawLineChart(args.folder + '/ardrone_1/payloadpose.txt', 3, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawLineChart(args.folder + '/ardrone_2/payloadpose.txt', 3, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawLineChart(args.folder + '/ardrone_1/payloadpose_truth.txt', 3, 'ground truth', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    # plt.legend(loc='upper right')
    plt.ylabel('z')
    
    plt.subplot(6, 1, 4)
    DrawLineChart(args.folder + '/ardrone_1/payloadpose.txt', 4, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawLineChart(args.folder + '/ardrone_2/payloadpose.txt', 4, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawLineChart(args.folder + '/ardrone_1/payloadpose_truth.txt', 4, 'ground truth', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    # plt.legend(loc='upper right')
    plt.ylabel('$\phi$ angle (degree)')

    plt.subplot(6, 1, 5)
    DrawLineChart(args.folder + '/ardrone_1/payloadpose.txt', 5, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawLineChart(args.folder + '/ardrone_2/payloadpose.txt', 5, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawLineChart(args.folder + '/ardrone_1/payloadpose_truth.txt', 5, 'ground truth', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    # plt.legend(loc='upper right')
    plt.ylabel('$\\theta$ angle (degree)')
    
    plt.subplot(6, 1, 6)
    DrawLineChart(args.folder + '/ardrone_1/payloadpose.txt', 6, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawLineChart(args.folder + '/ardrone_2/payloadpose.txt', 6, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawLineChart(args.folder + '/ardrone_1/payloadpose_truth.txt', 6, 'ground truth', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    # plt.legend(loc='upper right')
    plt.ylabel('$\psi$ angle (degree)')

    plt.xlabel('time (s)')

    plot = plt.subplot(6, 1, 1)
    plot.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)

    if config['mainAxis'] >= 0:
        axisName = ['x', 'y', 'z']
        axisLimit = [[config['xmin'], config['xmax']], [config['ymin'], config['ymax']], [config['zmin'], config['zmax']]]

        fig = plt.figure(figsize=(10, 6))
        count = 1
        for otherAxis in range(3):
            if otherAxis != config['mainAxis']:
                plt.subplot(2, 1, count)
                plt.xlim(axisLimit[config['mainAxis']])
                plt.ylim(axisLimit[otherAxis])
                DrawTrajectory(args.folder + '/trajectory.txt', config['mainAxis'], otherAxis, 'trajectory')
                DrawLineChart2(args.folder + '/ardrone_1/payloadpose_truth.txt', config['mainAxis'] + 1, otherAxis + 1, 'payload_position', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
                # plt.legend(loc='upper right')
                plt.xlabel(axisName[config['mainAxis']])
                plt.ylabel(axisName[otherAxis])
                count += 1

        plot = plt.subplot(2, 1, 1)
        plot.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)

    # fig = plt.figure()
    # plot = Axes3D(fig)
    # plot.set_xlim3d(-1, 11)
    # plot.set_ylim3d(-1, 1)
    # plot.set_zlim3d(0, 2)
    # Draw3DTrajectory(args.folder + '/trajectory.txt', plot, color=COLOR_BLUE)
    # Draw3DPosition(args.folder + '/ardrone_1/payloadpose_truth.txt', plot, color=COLOR_GREEN, startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])

    fig = plt.figure(figsize=(10, 12))

    plt.subplot(6, 1, 1)
    plt.ylim(-1, 1)
    DrawDroneEstimationError(args.folder + '/ardrone_1', 1, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawDroneEstimationError(args.folder + '/ardrone_2', 1, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    # plt.legend(loc='upper right')
    plt.ylabel('x')

    plt.subplot(6, 1, 2)
    plt.ylim(-1, 1)
    DrawDroneEstimationError(args.folder + '/ardrone_1', 2, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawDroneEstimationError(args.folder + '/ardrone_2', 2, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    # plt.legend(loc='upper right')
    plt.ylabel('y')

    plt.subplot(6, 1, 3)
    plt.ylim(-1, 1)
    DrawDroneEstimationError(args.folder + '/ardrone_1', 3, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawDroneEstimationError(args.folder + '/ardrone_2', 3, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    # plt.legend(loc='upper right')
    plt.ylabel('z')

    plt.subplot(6, 1, 4)
    plt.ylim(-10, 10)
    DrawDroneEstimationError(args.folder + '/ardrone_1', 4, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawDroneEstimationError(args.folder + '/ardrone_2', 4, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    # plt.legend(loc='upper right')
    plt.ylabel('$\phi$ angle (degree)')

    plt.subplot(6, 1, 5)
    plt.ylim(-10, 10)
    DrawDroneEstimationError(args.folder + '/ardrone_1', 5, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawDroneEstimationError(args.folder + '/ardrone_2', 5, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    # plt.legend(loc='upper right')
    plt.ylabel('$\\theta$ angle (degree)')

    plt.subplot(6, 1, 6)
    plt.ylim(-10, 10)
    DrawDroneEstimationError(args.folder + '/ardrone_1', 6, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    DrawDroneEstimationError(args.folder + '/ardrone_2', 6, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    # plt.legend(loc='upper right')
    plt.ylabel('$\psi$ angle (degree)')

    plt.xlabel('time (s)')

    plot = plt.subplot(6, 1, 1)
    plot.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)

    meanErrors, maxErrors = DroneEstimationError(args.folder + '/ardrone_1', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    print('drone 1 estimation error:')
    print('mean: ' + str(meanErrors[0]) + '\t' + str(meanErrors[1]) + '\t' + str(meanErrors[2]) + '\t' + str(meanErrors[3]) + '\t' + str(meanErrors[4]) + '\t' + str(meanErrors[5]))
    print('max: ' + str(maxErrors[0]) + '\t' + str(maxErrors[1]) + '\t' + str(maxErrors[2]) + '\t' + str(maxErrors[3]) + '\t' + str(maxErrors[4]) + '\t' + str(maxErrors[5]))

    meanErrors, maxErrors = DroneEstimationError(args.folder + '/ardrone_2', startTime=config['startTime'], endTime=config['endTime'], step=config['timeStep'])
    print('drone 2 estimation error:')
    print('mean: ' + str(meanErrors[0]) + '\t' + str(meanErrors[1]) + '\t' + str(meanErrors[2]) + '\t' + str(meanErrors[3]) + '\t' + str(meanErrors[4]) + '\t' + str(meanErrors[5]))
    print('max: ' + str(maxErrors[0]) + '\t' + str(maxErrors[1]) + '\t' + str(maxErrors[2]) + '\t' + str(maxErrors[3]) + '\t' + str(maxErrors[4]) + '\t' + str(maxErrors[5]))

    plt.show()