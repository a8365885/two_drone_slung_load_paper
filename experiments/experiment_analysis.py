#!/usr/bin/env python

import argparse
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import json
import math
import os

COLOR_BLUE = 'b'
COLOR_GREEN = 'g'
COLOR_RED = 'r'
COLOR_CYAN = 'c'
COLOR_MAGENTA = 'm'
COLOR_YELLOW = 'y'
COLOR_BLACK = 'k'
COLOR_WHITE = 'w'
COLORS = [COLOR_BLUE, COLOR_GREEN, COLOR_RED, COLOR_CYAN, COLOR_MAGENTA, COLOR_YELLOW, COLOR_BLACK, COLOR_WHITE]

def GetTrajectory(folder):
    filePath = folder + '/trajectory.txt'
    datas = []
    with open(filePath, 'r') as f:
        lines = f.readlines()
        for line in lines:
            data = []
            valuesStr = line.split('\t')
            data.append(float(valuesStr[0]))
            data.append(float(valuesStr[1]))
            data.append(float(valuesStr[2]))
            datas.append(data)
    return datas

def GetTimestamp(folder, dronename):
    timestampfiles = [folder + '/' + dronename + '_1/timestamp.txt', folder + '/' + dronename + '_2/timestamp.txt']
    timestamps = []
    for timestampfile in timestampfiles:
        timestampdrone = []
        with open(timestampfile, 'r') as f:
            lines = f.readlines()
            lines.pop(0)
            for line in lines:
                line = line.replace('\n', '')
                line = line.replace('\r', '')
                strs = line.split('\t')
                if strs[2] == 'done':
                    timestampdrone.append(float(strs[0]))
        timestamps.append(timestampdrone)
    timestamp = np.array(timestamps).min(0)
    return timestamp.tolist()

def GetConfig(folder, dronename):
    try:
        with open(folder + '/config.json', 'r') as f:
            config = json.load(f)
        if not('timestamp_waypoint' in config) or not('timestamp_part' in config) or not('startTime' in config) or not('endTime' in config):
            raise IOError
    except IOError:
        config = {}
        alltimestamp = GetTimestamp(folder, dronename)
        timestampindexs = [0, 23, 27, 28, 34]
        timestamp = []
        for timestampindex in timestampindexs:
            timestamp.append(alltimestamp[timestampindex])
        
        config['timestamp_waypoint'] = alltimestamp
        config['timestamp_part'] = timestamp
        config['startTime'] = float(timestamp[0])
        config['endTime'] = float(timestamp[len(timestamp) - 1])

        with open(folder + '/config.json', 'w') as f:
            json.dump(config, f, indent=4)
    return config

def GetFileData(filePath, startTime=15., endTime=35., step=0.1):
    data = []

    stepTime = 0
    with open(filePath, 'r') as f:
        lines = f.readlines()
        for line in lines:
            valuesStr = line.split('\t')
            time = float(valuesStr[0])
            if (time >= startTime + stepTime) & (time <= endTime):
                data.append([
                    float(valuesStr[0]) - startTime, 
                    float(valuesStr[1]), 
                    float(valuesStr[2]), 
                    float(valuesStr[3]), 
                    float(valuesStr[4]), 
                    float(valuesStr[5]), 
                    float(valuesStr[6])])
                stepTime += step
    return data

def GetDroneEstimationPayloadPose(droneFolder, startTime=15., endTime=35., step=0.1):
    filePath = droneFolder + '/payloadpose.txt'
    filePath_truth = droneFolder + '/payloadpose_truth.txt'
    payloadpose = GetFileData(filePath, startTime, endTime, step)
    payloadpose_truth = GetFileData(filePath_truth, startTime, endTime, step)
    return [payloadpose, payloadpose_truth]

def GetDronePose(droneFolder, startTime=15., endTime=35., step=0.1):
    filePath = droneFolder + '/dronepose.txt'
    dronepose = GetFileData(filePath, startTime, endTime, step)
    return dronepose

def GetMarkerPose(droneFolder, startTime=15., endTime=35., step=0.1):
    filePath = droneFolder + '/markerpose_truth.txt'
    markerpose_truth = GetFileData(filePath, startTime, endTime, step)
    return markerpose_truth

def FilterMaxYawEstimationError(payloadpose, payloadpose_truth, percentage=1.0):
    times = [row[0] for row in payloadpose]
    datas = []
    for t in range(len(times)):
        data = []
        data.extend(payloadpose[t])
        data.extend(payloadpose_truth[t])
        data.append(abs(payloadpose[t][6] - payloadpose_truth[t][6]))
        datas.append(data)
    datas.sort(key=lambda x:abs(x[14]))
    newLen = int(len(datas) * percentage)
    datas = datas[0:newLen]
    return [[row[0:7] for row in datas], [row[7:14] for row in datas]]

def CalcDroneEstimationError(payloadpose, payloadpose_truth):
    allErrors = []
    times = [row[0] for row in payloadpose]
    for t in range(len(times)):
        errors = [times[t]]
        for i in range(6):
            error = payloadpose[t][i + 1] - payloadpose_truth[t][i + 1]
            errors.append(error)
        allErrors.append(errors)

    meanErrors = []
    maxErrors = []
    for i in range(6):
        data = [abs(n) for n in [row[i + 1] for row in allErrors]]
        data.sort(reverse=True)

        sumError = sum(data)
        meanError = sumError / len(data)
        maxError = data[0]

        meanErrors.append(meanError)
        maxErrors.append(maxError)
    return meanErrors, maxErrors

def CalcPayloadPitch(payloadpose_truth):
    data = [abs(n) for n in [row[5] for row in payloadpose_truth]]
    data.sort(reverse=True)

    sumPayloadPitch = sum(data)
    meanPayloadPitch = sumPayloadPitch / len(data)
    maxPayloadPitch = data[0]

    return meanPayloadPitch, maxPayloadPitch

def CalcSwingAngle(dronepose, markerpose, cablelength):
    allSwingAngle = []
    times = [row[0] for row in dronepose]
    for t in range(len(times)):
        dx = dronepose[t][1]
        mx = markerpose[t][1]
        l = dx - mx
        angle = math.atan2(l, cablelength) * 180 / math.pi

        swingAngle = [times[t], angle]
        allSwingAngle.append(swingAngle)

    data = [abs(n) for n in [row[1] for row in allSwingAngle]]
    data.sort(reverse=True)

    sumSwingAngle = sum(data)
    meanSwingAngle = sumSwingAngle / len(data)
    maxSwingAngle = data[0]

    return meanSwingAngle, maxSwingAngle

def CalcSwingAngle2(dronepose, markerpose, cablelength):
    allSwingAngle = []
    times = [row[0] for row in dronepose]
    for t in range(len(times)):
        dx = dronepose[t][1]
        dy = dronepose[t][2]
        mx = markerpose[t][1]
        my = markerpose[t][2]
        l = math.sqrt((dx - mx) * (dx - mx) + (dy - my) * (dy - my))
        angle = math.atan2(l, cablelength) * 180 / math.pi

        swingAngle = [times[t], angle]
        allSwingAngle.append(swingAngle)

    data = [abs(n) for n in [row[1] for row in allSwingAngle]]
    data.sort(reverse=True)

    sumSwingAngle = sum(data)
    meanSwingAngle = sumSwingAngle / len(data)
    maxSwingAngle = data[0]

    return meanSwingAngle, maxSwingAngle

def CalcSpendTime(time):
    time.sort(reverse=True)
    sumTime = sum(time)
    meanTime = sumTime / len(time)
    maxTime = time[0]
    return meanTime, maxTime

def Point2LineDistance(p, l1, l2):
    l = l2 - l1
    return np.linalg.norm(np.outer(np.dot(p - l1, l) / np.dot(l, l), l) + l1 - p, axis=1)

def Point2LineDistance2D(p, l1, l2):
    l = l2 - l1
    return np.linalg.norm(np.outer(np.dot(p - l1, l) / np.dot(l, l), l) + l1 - p, axis=1)

def TrajectoryDeviation(payloadpose, trajectory, timestamp):
    timestamp = np.array(timestamp) - timestamp[0]
    deviations = []
    timestep = 1
    for pose in payloadpose:
        if pose[0] > timestamp[timestep]:
            timestep += 1
        deviation = Point2LineDistance(
            np.array(pose[1:4]), 
            np.array(trajectory[timestep - 1][0:3]), 
            np.array(trajectory[timestep][0:3]))[0]
        deviations.append([pose[0], deviation])
    return deviations

def TrajectoryDeviation2D(payloadpose, trajectory, timestamp):
    timestamp = np.array(timestamp) - timestamp[0]
    deviations = []
    timestep = 1
    for pose in payloadpose:
        if pose[0] > timestamp[timestep]:
            timestep += 1
        deviation = Point2LineDistance2D(
            np.array(pose[1:3]), 
            np.array(trajectory[timestep - 1][0:2]), 
            np.array(trajectory[timestep][0:2]))[0]
        deviations.append([pose[0], deviation])
    return deviations

def CalcTrajectoryDeviation(deviation):
    data = [abs(n) for n in [row[1] for row in deviation]]
    data.sort(reverse=True)

    sumTrajectoryDeviation = sum(data)
    meanTrajectoryDeviation = sumTrajectoryDeviation / len(data)
    maxTrajectoryDeviation = data[0]

    return meanTrajectoryDeviation, maxTrajectoryDeviation

if __name__ == '__main__':
    argParser = argparse.ArgumentParser()
    argParser.add_argument('--folder', type=str, default='.')
    argParser.add_argument('--timeStep', type=float, default=0.0)
    argParser.add_argument('--ids', '-ids', type=int, nargs='+')
    argParser.add_argument('--cablelength', '-cl', type=float, default=0.4)
    argParser.add_argument('--cablelengths', '-cls', type=float, nargs='+')
    argParser.add_argument('--subidcount', '-sidc', type=int, default=10)
    argParser.add_argument('--subids', '-sids', type=int, nargs='+')
    args = argParser.parse_args()

    all_payloadpose = []
    all_payloadpose_truth = []
    all_dronepose = []
    all_markerpose = []
    all_time = []
    all_trajectorydeviation = []
    all_trajectorydeviation_2d = []
    for id in args.ids:
        dronename = 'ardrone' if id < 200 else 'drone'
        experiment_payloadpose = []
        experiment_payloadpose_truth = []
        experiment_dronepose = []
        experiment_markerpose = []
        experiment_time = []
        experiment_trajectorydeviation = []
        experiment_trajectorydeviation_2d = []
        subids = args.subids if len(args.subids) == args.subidcount else range(1, args.subidcount + 1)
        for subid in subids:
            experiment_folder = args.folder + '/research_experiment_' + str(id) + '_' + str(subid)
            config = GetConfig(experiment_folder, dronename)
            starttime = config['startTime']
            endtime = config['endTime']
            timestep = args.timeStep
            timestamp_waypoint = config['timestamp_waypoint']
            trajectory = GetTrajectory(experiment_folder)

            payloadpose, payloadpose_truth = GetDroneEstimationPayloadPose(experiment_folder + '/' + dronename + '_1', startTime=starttime, endTime=endtime, step=timestep)
            experiment_payloadpose.extend(payloadpose)
            experiment_payloadpose_truth.extend(payloadpose_truth)
            payloadpose, payloadpose_truth = GetDroneEstimationPayloadPose(experiment_folder + '/' + dronename + '_2', startTime=starttime, endTime=endtime, step=timestep)
            experiment_payloadpose.extend(payloadpose)
            experiment_payloadpose_truth.extend(payloadpose_truth)

            dronepose = GetDronePose(experiment_folder + '/' + dronename + '_1', startTime=starttime, endTime=endtime, step=timestep)
            markerpose = GetMarkerPose(experiment_folder + '/' + dronename + '_1', startTime=starttime, endTime=endtime, step=timestep)
            experiment_dronepose.extend(dronepose)
            experiment_markerpose.extend(markerpose)
            dronepose = GetDronePose(experiment_folder + '/' + dronename + '_2', startTime=starttime, endTime=endtime, step=timestep)
            markerpose = GetMarkerPose(experiment_folder + '/' + dronename + '_2', startTime=starttime, endTime=endtime, step=timestep)
            experiment_dronepose.extend(dronepose)
            experiment_markerpose.extend(markerpose)

            experiment_time.append(endtime - starttime)
            
            trajectorydeviation = TrajectoryDeviation(payloadpose_truth, trajectory, timestamp_waypoint)
            experiment_trajectorydeviation.extend(trajectorydeviation)
            trajectorydeviation2d = TrajectoryDeviation2D(payloadpose_truth, trajectory, timestamp_waypoint)
            experiment_trajectorydeviation_2d.extend(trajectorydeviation2d)
        all_payloadpose.append(experiment_payloadpose)
        all_payloadpose_truth.append(experiment_payloadpose_truth)
        all_dronepose.append(experiment_dronepose)
        all_markerpose.append(experiment_markerpose)
        all_time.append(experiment_time)
        all_trajectorydeviation.append(experiment_trajectorydeviation)
        all_trajectorydeviation_2d.append(experiment_trajectorydeviation_2d)

    str_ids = "{:<8}".format("")
    str_payloadpose_count = "{:<8}".format("")
    str_payloadpose_truth_count = "{:<8}".format("")
    str_yaw_error_90_count = "{:<8}".format("")
    str_yaw_error_90_ratio = "{:<8}".format("")
    str_estimation_error_100_mean = ["{:<8}".format("x"), "{:<8}".format("y"), "{:<8}".format("z"), "{:<8}".format("$\phi$"), "{:<8}".format("$\\theta$"), "{:<8}".format("$\psi$")]
    str_estimation_error_100_max = ["{:<8}".format("x"), "{:<8}".format("y"), "{:<8}".format("z"), "{:<8}".format("$\phi$"), "{:<8}".format("$\\theta$"), "{:<8}".format("$\psi$")]
    str_estimation_error_99_mean = ["{:<8}".format("x"), "{:<8}".format("y"), "{:<8}".format("z"), "{:<8}".format("$\phi$"), "{:<8}".format("$\\theta$"), "{:<8}".format("$\psi$")]
    str_estimation_error_99_max = ["{:<8}".format("x"), "{:<8}".format("y"), "{:<8}".format("z"), "{:<8}".format("$\phi$"), "{:<8}".format("$\\theta$"), "{:<8}".format("$\psi$")]
    str_payload_pitch_mean = "{:<8}".format("mean")
    str_payload_pitch_max = "{:<8}".format("max")
    str_swing_angle_mean = "{:<8}".format("mean")
    str_swing_angle_max = "{:<8}".format("max")
    str_swing_angle_2_mean = "{:<8}".format("mean")
    str_swing_angle_2_max = "{:<8}".format("max")
    str_spend_time_mean =  "{:<8}".format("mean")
    str_spend_time_max =  "{:<8}".format("max")
    str_trajectory_deviation_mean =  "{:<8}".format("mean")
    str_trajectory_deviation_max =  "{:<8}".format("max")
    str_trajectory_deviation_2d_mean =  "{:<8}".format("mean")
    str_trajectory_deviation_2d_max =  "{:<8}".format("max")

    for index in range(len(args.ids)):
        str_ids += " & " + "{:<8}".format(str(args.ids[index]))
        str_payloadpose_count += " & " + "{:<8d}".format(len(all_payloadpose[index]))
        str_payloadpose_truth_count += " & " + "{:<8d}".format(len(all_payloadpose_truth[index]))

        yaw_error_90_count = 0
        yaw_error_90_ratio = 0
        for t in range(len(all_payloadpose[index])):
            yaw_error = all_payloadpose[index][t][6] - all_payloadpose_truth[index][t][6]
            if abs(yaw_error) >= 90:
                yaw_error_90_count += 1
        yaw_error_90_ratio = float(yaw_error_90_count) / float(len(all_payloadpose[index]))
        str_yaw_error_90_count += " & " + "{:<8d}".format(yaw_error_90_count)
        str_yaw_error_90_ratio += " & " + "{:0.6f}".format(yaw_error_90_ratio)
    
        filter100_payloadpose, filter100_payloadpose_truth = FilterMaxYawEstimationError(all_payloadpose[index], all_payloadpose_truth[index], percentage=1.0)
        meanErrors, maxErrors = CalcDroneEstimationError(filter100_payloadpose, filter100_payloadpose_truth)
        for i in range(6):
            str_estimation_error_100_mean[i] += " & " + "{:8.4f}".format(meanErrors[i])
            str_estimation_error_100_max[i] += " & " + "{:8.4f}".format(maxErrors[i])
    
        filter99_payloadpose, filter99_payloadpose_truth = FilterMaxYawEstimationError(all_payloadpose[index], all_payloadpose_truth[index], percentage=0.99 if yaw_error_90_ratio <= 0.01 else 1.0 - yaw_error_90_ratio)
        meanErrors, maxErrors = CalcDroneEstimationError(filter99_payloadpose, filter99_payloadpose_truth)
        for i in range(6):
            str_estimation_error_99_mean[i] += " & " + "{:8.4f}".format(meanErrors[i])
            str_estimation_error_99_max[i] += " & " + "{:8.4f}".format(maxErrors[i])
        
        meanPayloadPitch, maxPayloadPitch = CalcPayloadPitch(all_payloadpose_truth[index])
        str_payload_pitch_mean += " & " + "{:8.4f}".format(meanPayloadPitch)
        str_payload_pitch_max += " & " + "{:8.4f}".format(maxPayloadPitch)

        cablelength = args.cablelengths[index] if args.cablelengths and len(args.cablelengths) == len(args.ids) else args.cablelength

        meanSwingAngle, maxSwingAngle = CalcSwingAngle(all_dronepose[index], all_markerpose[index], cablelength)
        str_swing_angle_mean += " & " + "{:8.4f}".format(meanSwingAngle)
        str_swing_angle_max += " & " + "{:8.4f}".format(maxSwingAngle)

        meanSwingAngle, maxSwingAngle = CalcSwingAngle2(all_dronepose[index], all_markerpose[index], cablelength)
        str_swing_angle_2_mean += " & " + "{:8.4f}".format(meanSwingAngle)
        str_swing_angle_2_max += " & " + "{:8.4f}".format(maxSwingAngle)

        meanTime, maxTime = CalcSpendTime(all_time[index])
        str_spend_time_mean  += " & " + "{:8.4f}".format(meanTime)
        str_spend_time_max  += " & " + "{:8.4f}".format(maxTime)

        meanTrajectoryDeviation, maxTrajectoryDeviation = CalcTrajectoryDeviation(all_trajectorydeviation[index])
        str_trajectory_deviation_mean  += " & " + "{:8.4f}".format(meanTrajectoryDeviation)
        str_trajectory_deviation_max  += " & " + "{:8.4f}".format(maxTrajectoryDeviation)
        meanTrajectoryDeviation, maxTrajectoryDeviation = CalcTrajectoryDeviation(all_trajectorydeviation_2d[index])
        str_trajectory_deviation_2d_mean  += " & " + "{:8.4f}".format(meanTrajectoryDeviation)
        str_trajectory_deviation_2d_max  += " & " + "{:8.4f}".format(maxTrajectoryDeviation)

    output = ''
    
    output += "ids:\n" + str_ids + '\n'
    output += '\n'
    output += "payloadpose count:\n" + str_payloadpose_count + '\n'
    output += '\n'
    output += "payloadpose truth count:\n" + str_payloadpose_truth_count + '\n'
    output += '\n'
    output += "yaw error >= 90 count:\n" + str_yaw_error_90_count + '\n'
    output += '\n'
    output += "yaw error >= 90 ratio:\n" + str_yaw_error_90_ratio + '\n'
    output += '\n'
    output += "all estimation error mean:" + '\n'
    for i in range(6):
        output += str_estimation_error_100_mean[i] + '\n'
    output += '\n'
    output += "all estimation error max:" + '\n'
    for i in range(6):
        output += str_estimation_error_100_max[i] + '\n'
    output += '\n'
    output += "99% estimation error mean:" + '\n'
    for i in range(6):
        output += str_estimation_error_99_mean[i] + '\n'
    output += '\n'
    output += "99% estimation error max:" + '\n'
    for i in range(6):
        output += str_estimation_error_99_max[i] + '\n'
    output += '\n'
    output += "payload pitch:" + '\n'
    output += str_payload_pitch_mean + '\n'
    output += str_payload_pitch_max + '\n'
    output += '\n'
    output += "swing angle:" + '\n'
    output += str_swing_angle_mean + '\n'
    output += str_swing_angle_max + '\n'
    output += '\n'
    output += "swing angle 2:" + '\n'
    output += str_swing_angle_2_mean + '\n'
    output += str_swing_angle_2_max + '\n'
    output += '\n'
    output += "spend time:" + '\n'
    output += str_spend_time_mean + '\n'
    output += str_spend_time_max + '\n'
    output += '\n'
    output += "trajectory deviation:" + '\n'
    output += str_trajectory_deviation_mean + '\n'
    output += str_trajectory_deviation_max + '\n'
    output += '\n'
    output += "trajectory deviation 2d:" + '\n'
    output += str_trajectory_deviation_2d_mean + '\n'
    output += str_trajectory_deviation_2d_max + '\n'
    output += '\n'
    
    print(output)

    title = 'exp'
    for id in args.ids:
        title += '_' + str(id)
    outputfilename = args.folder + '/output/' + title + '.txt'
    os.makedirs(os.path.dirname(outputfilename), exist_ok=True)
    with open(outputfilename, 'w') as f:
        f.write(output)
