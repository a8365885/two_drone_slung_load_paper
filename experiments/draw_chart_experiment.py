#!/usr/bin/env python

import argparse
import numpy as np
import matplotlib.pyplot as plt
import json
from mpl_toolkits.mplot3d import Axes3D
from itertools import product, combinations
import math

COLOR_BLUE = 'b'
COLOR_GREEN = 'g'
COLOR_RED = 'r'
COLOR_CYAN = 'c'
COLOR_MAGENTA = 'm'
COLOR_YELLOW = 'y'
COLOR_BLACK = 'k'
COLOR_WHITE = 'w'
COLORS = [COLOR_BLUE, COLOR_GREEN, COLOR_RED, COLOR_CYAN, COLOR_MAGENTA, COLOR_YELLOW, COLOR_BLACK, COLOR_WHITE]

def GetTimestamp(folder, dronename):
    timestampfiles = [folder + '/' + dronename + '_1/timestamp.txt', folder + '/' + dronename + '_2/timestamp.txt']
    timestamps = []
    for timestampfile in timestampfiles:
        timestampdrone = []
        with open(timestampfile, 'r') as f:
            lines = f.readlines()
            lines.pop(0)
            for line in lines:
                line = line.replace('\n', '')
                line = line.replace('\r', '')
                strs = line.split('\t')
                if strs[2] == 'done':
                    timestampdrone.append(float(strs[0]))
        timestamps.append(timestampdrone)
    timestamp = np.array(timestamps).min(0)
    return timestamp.tolist()

def GetConfig(folder, dronename):
    try:
        with open(folder + '/config.json', 'r') as f:
            config = json.load(f)
        if not('timestamp_waypoint' in config) or not('timestamp_part' in config) or not('startTime' in config) or not('endTime' in config):
            raise IOError
    except IOError:
        config = {}
        alltimestamp = GetTimestamp(folder, dronename)
        timestampindexs = [0, 23, 27, 28, 34]
        timestamp = []
        for timestampindex in timestampindexs:
            timestamp.append(alltimestamp[timestampindex])
        
        config['timestamp_waypoint'] = alltimestamp
        config['timestamp_part'] = timestamp
        config['startTime'] = float(timestamp[0])
        config['endTime'] = float(timestamp[len(timestamp) - 1])

        with open(folder + '/config.json', 'w') as f:
            json.dump(config, f, indent=4)
    return config

def GetFileData(filePath, startTime=15., endTime=35., step=0.1):
    data = []

    stepTime = 0
    with open(filePath, 'r') as f:
        lines = f.readlines()
        for line in lines:
            valuesStr = line.split('\t')
            time = float(valuesStr[0])
            if (time >= startTime + stepTime) & (time <= endTime):
                data.append([
                    float(valuesStr[0]) - startTime, 
                    float(valuesStr[1]), 
                    float(valuesStr[2]), 
                    float(valuesStr[3]), 
                    float(valuesStr[4]), 
                    float(valuesStr[5]), 
                    float(valuesStr[6])])
                stepTime += step
    return data

def DrawLineChart(filePath, dataCol, dtatName, startTime=15., endTime=35., step=0.1):
    data = GetFileData(filePath, startTime, endTime, step)
    x = [row[0] for row in data]
    y = [row[dataCol] for row in data]
    plt.plot(x, y, '-', label=dtatName)

def DrawLineChart2(filePath, dataCol1, dataCol2, dtatName, startTime=15., endTime=35., step=0.1):
    data = GetFileData(filePath, startTime, endTime, step)
    x = [row[dataCol1] for row in data]
    y = [row[dataCol2] for row in data]
    plt.plot(x, y, '-', label=dtatName)

def DrawLineChart3(data, dataColx, dataColy, dtatName, color=None, linestyle='-', linewidth=1, marker=None):
    x = [row[dataColx] for row in data]
    y = [row[dataColy] for row in data]
    plt.plot(x, y, label=dtatName, color=color, linestyle=linestyle, linewidth=linewidth, marker=marker)

def DrawTrajectory(filePath, dataCol1, dataCol2, dtatName):
    x = []
    y = []

    with open(filePath, 'r') as f:
        lines = f.readlines()
        for line in lines:
            valuesStr = line.split('\t')
            x.append(float(valuesStr[dataCol1]))
            y.append(float(valuesStr[dataCol2]))

    plt.plot(x, y, '.-', label=dtatName)

def Draw3DPosition(filePath, plot, label, color=None, startTime=15., endTime=35., step=0.1):
    data = GetFileData(filePath, startTime, endTime, step)
    x = [row[1] for row in data]
    y = [row[2] for row in data]
    z = [row[3] for row in data]
    plot.plot(np.array(x), np.array(y), zs=np.array(z), c=color, label=label)
    return None

def GetTrajectory(folder):
    filePath = folder + '/trajectory.txt'
    datas = []
    with open(filePath, 'r') as f:
        lines = f.readlines()
        for line in lines:
            data = []
            valuesStr = line.split('\t')
            data.append(float(valuesStr[0]))
            data.append(float(valuesStr[1]))
            data.append(float(valuesStr[2]))
            data.append(float(valuesStr[3]))
            data.append(float(valuesStr[4]))
            data.append(float(valuesStr[5]))
            datas.append(data)
    return datas

def Draw3DTrajectory(folder, plot, label, color=None):
    data = GetTrajectory(folder)
    x = [row[0] for row in data]
    y = [row[1] for row in data]
    z = [row[2] for row in data]

    plot.plot(np.array(x), np.array(y), zs=np.array(z), c=color, label=label)
    return None

def GetDroneEstimationError(folder, startTime=15., endTime=35., step=0.1):
    filePaths = [folder + '/payloadpose_truth.txt', folder + '/payloadpose.txt']
    datas = []
    for filePath in filePaths:
        data = GetFileData(filePath, startTime, endTime, step)
        datas.append(data)

    allErrors = []
    times = [[row[0] for row in datas[0]], [row[0] for row in datas[1]]]
    for t in range(len(times[0])):
        errors = [times[0][t]]
        for i in range(6):
            error = datas[0][t][i+1] - datas[1][t][i+1]
            errors.append(error)
        allErrors.append(errors)
    return allErrors

def CalcDroneEstimationError(allErrors):
    sumErrors = []
    maxErrors = []
    for i in range(6):
        sumError = 0
        maxError = 0
        for t in range(len(allErrors)):
            error = abs(allErrors[t][i + 1])
            sumError += error
            if error > maxError:
                maxError = error
        sumErrors.append(sumError)
        maxErrors.append(maxError)
    meanErrors = [item / len(allErrors) for item in sumErrors]
    return meanErrors, maxErrors


def DroneEstimationError(folder, startTime=15., endTime=35., step=0.1):
    allErrors = GetDroneEstimationError(folder, startTime, endTime, step)
    return CalcDroneEstimationError(allErrors)

def DrawDroneEstimationError(folder, dataCol, dtatName, startTime=15., endTime=35., step=0.1):
    allErrors = GetDroneEstimationError(folder, startTime, endTime, step)
    times = [row[0] for row in allErrors]
    errors = [row[dataCol] for row in allErrors]
    plt.plot(times, errors, '-', label=dtatName)
    # plt.bar(times, errors, label=dtatName)

def DrawDroneEstimationError_Position(folder, dtatName, startTime=15., endTime=35., step=0.1):
    allErrors = GetDroneEstimationError(folder, startTime, endTime, step)
    times = [row[0] for row in allErrors]
    xErrors = [row[1] for row in allErrors]
    yErrors = [row[2] for row in allErrors]
    zErrors = [row[3] for row in allErrors]
    # positionErrors = [math.sqrt(math.pow(xErrors[i], 2) + math.pow(yErrors[i], 2) + math.pow(zErrors[i], 2)) for i in range(len(allErrors))]
    positionErrors = [math.sqrt(x * x + y * y + z * z) for x, y, z in zip(xErrors, yErrors, zErrors)]
    # plt.plot(times, positionErrors, '-', label=dtatName)
    plt.bar(times, positionErrors, label=dtatName)

def DrawTimeStamp(timestamps, color='black', texty=0):
    for i in range(len(timestamps) - 2):
        plt.axvline(timestamps[i + 1] - timestamps[0], color=color)
        plt.text((timestamps[i + 1] + timestamps[i]) / 2 - timestamps[0], texty, '(' + str(i+1) + ')', ha='center', va='top')
    plt.text((timestamps[len(timestamps) - 1] + timestamps[len(timestamps) - 2]) / 2 - timestamps[0], texty, '(' + str(len(timestamps) - 1) + ')', ha='center', va='top')

def GetWindData(folder, startTime=15., endTime=35., step=0.1):
    filePath = folder + '/wind/wind.txt'
    data = []

    stepTime = 0
    with open(filePath, 'r') as f:
        lines = f.readlines()
        for line in lines:
            valuesStr = line.split('\t')
            time = float(valuesStr[0])
            if (time >= startTime + stepTime) & (time <= endTime):
                data.append([
                    float(valuesStr[0]) - startTime, 
                    float(valuesStr[1]), 
                    float(valuesStr[2]), 
                    float(valuesStr[3]), 
                    float(valuesStr[4])])
                stepTime += step
    return data

def DrawWindForceChart(folder, startTime=15., endTime=35., step=0.1):
    data = GetWindData(folder, startTime, endTime, step)
    times = [row[0] for row in data]
    speed = [row[1] for row in data]
    plt.plot(times, speed, '-', label='wind speed (n)')

def DrawWindDirectionChart(folder, startTime=15., endTime=35., step=0.1):
    data = GetWindData(folder, startTime, endTime, step)
    times = [row[0] for row in data]
    x = [row[2] for row in data]
    y = [row[3] for row in data]
    angle = np.arctan2(y, x) * 180 / np.pi
    plt.plot(times, angle, '-', label='angle')

def PointLineDistance(p, l1, l2):
    l = l2 - l1
    return np.linalg.norm(np.outer(np.dot(p - l1, l) / np.dot(l, l), l) + l1 - p, axis=1)

def PointLineDistance2D(p, l1, l2):
    l = l2 - l1
    return np.linalg.norm(np.outer(np.dot(p - l1, l) / np.dot(l, l), l) + l1 - p, axis=1)

def CalcTrajectoryError(trajectory, payloadpose, timestamp):
    timestamp = np.array(timestamp) - timestamp[0]
    errors = []
    timestep = 1
    for pose in payloadpose:
        if pose[0] > timestamp[timestep]:
            timestep += 1
        error = PointLineDistance(
            np.array(pose[1:4]), 
            np.array(trajectory[timestep - 1][0:3]), 
            np.array(trajectory[timestep][0:3]))[0]
        errors.append([pose[0], error])
    return errors

def CalcTrajectoryError2D(trajectory, payloadpose, timestamp):
    timestamp = np.array(timestamp) - timestamp[0]
    errors = []
    timestep = 1
    for pose in payloadpose:
        if pose[0] > timestamp[timestep]:
            timestep += 1
        error = PointLineDistance2D(
            np.array(pose[1:3]), 
            np.array(trajectory[timestep - 1][0:2]), 
            np.array(trajectory[timestep][0:2]))[0]
        errors.append([pose[0], error])
    return errors
        
def DrawTrajectoryError(folder, dronename, startTime=15., endTime=35., step=0.1):
    data = CalcTrajectoryError(
        GetTrajectory(folder), 
        GetFileData(folder + '/' + dronename + '_1/payloadpose_truth.txt', startTime, endTime, step), 
        GetTimestamp(folder, dronename))
    times = [row[0] for row in data]
    errors = [row[1] for row in data]
    plt.plot(times, errors, '-', label='error')
        
def DrawTrajectoryError2D(folder, dronename, startTime=15., endTime=35., step=0.1):
    data = CalcTrajectoryError2D(
        GetTrajectory(folder), 
        GetFileData(folder + '/' + dronename + '_1/payloadpose_truth.txt', startTime, endTime, step), 
        GetTimestamp(folder, dronename))
    times = [row[0] for row in data]
    errors = [row[1] for row in data]
    plt.plot(times, errors, '-', label='error')
    errors.sort(reverse=True)
    meanError = sum(errors) / len(errors)
    maxError = errors[0]
    return meanError, maxError

def DrawSwingAngle(dronepose, markerpose, cablelength, dtatName):
    times = [row[0] for row in dronepose]
    angles = []
    for t in range(len(times)):
        dx = dronepose[t][1]
        mx = markerpose[t][1]
        l = dx - mx
        angle = abs(math.atan2(l, cablelength) * 180 / math.pi)
        angles.append(angle)
    plt.plot(times, angles, '-', label=dtatName)
    angles.sort(reverse=True)
    meanAngle = sum(angles) / len(angles)
    maxAngle = angles[0]
    return meanAngle, maxAngle

def DrawSwingAngle2(dronepose, markerpose, cablelength, dtatName):
    times = [row[0] for row in dronepose]
    angles = []
    for t in range(len(times)):
        dx = dronepose[t][1]
        dy = dronepose[t][2]
        mx = markerpose[t][1]
        my = markerpose[t][2]
        l = math.sqrt((dx - mx) * (dx - mx) + (dy - my) * (dy - my))
        angle = math.atan2(l, cablelength) * 180 / math.pi
        angles.append(angle)
    plt.plot(times, angles, '-', label=dtatName)
    angles.sort(reverse=True)
    meanAngle = sum(angles) / len(angles)
    maxAngle = angles[0]
    return meanAngle, maxAngle

if __name__ == '__main__':
    argParser = argparse.ArgumentParser()
    argParser.add_argument('--folder', type=str, default='.')
    argParser.add_argument('--id', type=str, default='100')
    argParser.add_argument('--subid', type=str, default='1')
    argParser.add_argument('--wind', type=bool, default=False)
    argParser.add_argument('--fontsize', type=int, default=16) #10
    argParser.add_argument('--cablelength', '-cl', type=float, default=0.4)
    args = argParser.parse_args()

    experiment_folder = args.folder + '/research_experiment_' + args.id + '_' + args.subid
    dronename = 'ardrone' if int(args.id) < 200 else 'drone'

    config = GetConfig(experiment_folder, dronename)
    timeStep = 0
    cablelength = args.cablelength

    dronepose = []
    dronepose.append(GetFileData(experiment_folder + '/' + dronename + '_1/dronepose.txt', startTime=config['startTime'], endTime=config['endTime'], step=timeStep))
    dronepose.append(GetFileData(experiment_folder + '/' + dronename + '_2/dronepose.txt', startTime=config['startTime'], endTime=config['endTime'], step=timeStep))

    payloadpose_truth = []
    payloadpose_truth = GetFileData(experiment_folder + '/' + dronename + '_1/payloadpose_truth.txt', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)

    markerpose_truth = []
    markerpose_truth.append(GetFileData(experiment_folder + '/' + dronename + '_1/markerpose_truth.txt', startTime=config['startTime'], endTime=config['endTime'], step=timeStep))
    markerpose_truth.append(GetFileData(experiment_folder + '/' + dronename + '_2/markerpose_truth.txt', startTime=config['startTime'], endTime=config['endTime'], step=timeStep))

    trajectory = GetTrajectory(experiment_folder)
    trajectory_time = []
    for index in range(len(config['timestamp_waypoint'])):
        tp_time = [config['timestamp_waypoint'][index] - config['timestamp_waypoint'][0]]
        tp_time.extend(trajectory[index])
        trajectory_time.append(tp_time)

    plt.rcParams.update({'font.size': args.fontsize})

    ####################################################################################################

    fig = plt.figure(figsize=(10, 4))
    ylabels = ['Position (m)', 'Attitude (degree)']
    labels = ['x', 'y', 'z', '$\phi$', '$\\theta$', '$\psi$']
    ylims = [[-10, 10], [-100, 100]]
    for i in range(2):
        plot = plt.subplot(2, 1, 1 + i)
        for j in range(3):
            index = i * 3 + j
            DrawLineChart(experiment_folder + '/' + dronename + '_1/payloadpose_truth.txt', 1 + index, labels[index], startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
        DrawTimeStamp(config['timestamp_part'], texty=ylims[i][1]*0.9)
        plt.ylabel(ylabels[i])
        plt.ylim(ylims[i])
        plot.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)
    plt.xlabel('time (s)')
    plt.subplots_adjust(left=0.12, right=0.98, top=0.86, bottom=0.15, hspace=1.0)
    plt.savefig(experiment_folder + '/payload_pose.png')

    ####################################################################################################

    fig = plt.figure(figsize=(10, 2.4))
    labels = ['x', 'y', 'z']
    for i in range(3):
        DrawLineChart(experiment_folder + '/' + dronename + '_1/payloadpose_truth.txt', 1 + i, labels[i], startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    DrawTimeStamp(config['timestamp_part'], texty=10*0.9)
    plt.ylim(-10, 10)
    plt.ylabel('Position (m)')
    plt.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)
    plt.xlabel('time (s)')
    plt.subplots_adjust(left=0.12, right=0.98, top=0.76, bottom=0.26, hspace=0)
    plt.savefig(experiment_folder + '/payload_pose_position.png')

    ####################################################################################################

    fig = plt.figure(figsize=(10, 2.4))
    labels = ['$\phi$', '$\\theta$', '$\psi$']
    for i in range(3):
        DrawLineChart(experiment_folder + '/' + dronename + '_1/payloadpose_truth.txt', 4 + i, labels[i], startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    DrawTimeStamp(config['timestamp_part'], texty=100*0.9)
    plt.ylim(-100, 100)
    plt.ylabel('Attitude (degree)')
    plt.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)
    plt.xlabel('time (s)')
    plt.subplots_adjust(left=0.12, right=0.98, top=0.76, bottom=0.26, hspace=0)
    plt.savefig(experiment_folder + '/payload_pose_attitude.png')

    ####################################################################################################

    fig = plt.figure(figsize=(10, 8))
    ylabels = ['Position (m)', 'Attitude (degree)']
    labels = ['x', 'y', 'z', '$\phi$', '$\\theta$', '$\psi$']
    labels2 = ['x (path)', 'y (path)', 'z (path)', '$\phi$ (path)', '$\\theta$ (path)', '$\psi$ (path)']
    colors = ['b', 'c', 'g']
    ylims = [[-10, 10], [-100, 100]]
    for i in range(2):
        plot = plt.subplot(2, 1, 1 + i)
        for j in range(3):
            index = i * 3 + j
            DrawLineChart3(payloadpose_truth, 0, 1 + index, labels[index], color=colors[j], linestyle='-', linewidth=0.5)
            DrawLineChart3(trajectory_time, 0, 1 + index, labels2[index], color=colors[j], linestyle='--', linewidth=1)
        DrawTimeStamp(config['timestamp_part'], texty=ylims[i][1]*0.9)
        plt.ylabel(ylabels[i])
        plt.ylim(ylims[i])
        plot.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)
    plt.xlabel('time (s)')
    plt.subplots_adjust(left=0.12, right=0.98, top=0.89, bottom=0.08, hspace=0.5)
    plt.savefig(experiment_folder + '/payload_pose_2.png')

    ####################################################################################################

    fig = plt.figure(figsize=(10, 12))
    ylims = [[-10, 10], [-10, 10], [-10, 10], [-100, 100], [-100, 100], [-100, 100]]
    ylabels = ['x (m)', 'y (m)', 'z (m)', '$\phi$ (degree)', '$\\theta$ (degree)', '$\psi$ (degree)']
    for i in range(6):
        plt.subplot(6, 1, i + 1)
        DrawLineChart(experiment_folder + '/' + dronename + '_1/payloadpose.txt', i + 1, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
        DrawLineChart(experiment_folder + '/' + dronename + '_2/payloadpose.txt', i + 1, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
        DrawLineChart(experiment_folder + '/' + dronename + '_1/payloadpose_truth.txt', i + 1, 'ground truth', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
        DrawTimeStamp(config['timestamp_part'], texty=ylims[i][1]*0.9)
        plt.ylim(ylims[i])
        plt.ylabel(ylabels[i])
    plt.xlabel('time (s)')
    plot = plt.subplot(6, 1, 1)
    plot.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)
    plt.subplots_adjust(left=0.12, right=0.98, top=0.95, bottom=0.05, hspace=0.4)
    plt.savefig(experiment_folder + '/payload_pose_estimation.png')

    ####################################################################################################

    # fig = plt.figure(figsize=(15, 6))
    # ylims = [[-10, 10], [-10, 10], [-10, 10], [-100, 100], [-100, 100], [-100, 100]]
    # ylabels = ['x (m)', 'y (m)', 'z (m)', '$\phi$ (degree)', '$\\theta$ (degree)', '$\psi$ (degree)']
    # for i in range(6):
    #     plt.subplot(6, 1, i + 1)
    #     DrawLineChart(experiment_folder + '/' + dronename + '_1/payloadpose.txt', i + 1, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    #     DrawLineChart(experiment_folder + '/' + dronename + '_2/payloadpose.txt', i + 1, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    #     DrawLineChart(experiment_folder + '/' + dronename + '_1/payloadpose_truth.txt', i + 1, 'ground truth', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    #     DrawTimeStamp(config['timestamp_part'], texty=ylims[i][1]*0.9)
    #     plt.ylim(ylims[i])
    #     plt.ylabel(ylabels[i])
    # plt.xlabel('time (s)')
    # plot = plt.subplot(6, 1, 1)
    # plot.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)
    # plt.subplots_adjust(left=0.06, right=0.99, top=0.94, bottom=0.05, hspace=0.4)
    # plt.savefig(experiment_folder + '/payload_pose_estimation2.png')

    ####################################################################################################

    fig = plt.figure(figsize=(10, 6))
    plot = Axes3D(fig)
    plot.set_xlim3d(-5, 5)
    plot.set_ylim3d(-1, 9)
    plot.set_zlim3d(0, 4)
    Draw3DTrajectory(experiment_folder, plot, 'Desired Trajectory', color=COLOR_BLUE)
    Draw3DPosition(experiment_folder + '/' + dronename + '_1/payloadpose_truth.txt', plot, 'Payload Trajectory', color=COLOR_GREEN, startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    # plot.legend()
    plt.savefig(experiment_folder + '/payload_position_3d.png')

    ####################################################################################################

    # fig = plt.figure()
    # DrawTrajectory(experiment_folder + '/trajectory.txt', 0, 1, 'trajectory')
    # DrawLineChart2(experiment_folder + '/' + dronename + '_1/payloadpose_truth.txt', 1, 2, 'payload', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)

    ####################################################################################################

    fig = plt.figure(figsize=(10, 12))
    ylims = [[-0.1, 0.1], [-0.1, 0.1], [-0.1, 0.1], [-10, 10], [-10, 10], [-10, 10]]
    ylabels = ['x (m)', 'y (m)', 'z (m)', '$\phi$ (degree)', '$\\theta$ (degree)', '$\psi$ (degree)']
    for i in range(6):
        plt.subplot(6, 1, i + 1)
        DrawDroneEstimationError(experiment_folder + '/' + dronename + '_1', i + 1, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
        DrawDroneEstimationError(experiment_folder + '/' + dronename + '_2', i + 1, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
        DrawTimeStamp(config['timestamp_part'], texty=ylims[i][1]*0.9)
        plt.ylim(ylims[i])
        plt.ylabel(ylabels[i])
    plt.xlabel('time (s)')
    plot = plt.subplot(6, 1, 1)
    plot.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)
    plt.subplots_adjust(left=0.12, right=0.98, top=0.95, bottom=0.05, hspace=0.4)
    plt.savefig(experiment_folder + '/payload_pose_estimation_error.png')

    ####################################################################################################

    # fig = plt.figure(figsize=(15, 6))
    # ylims = [[-0.1, 0.1], [-0.1, 0.1], [-0.1, 0.1], [-10, 10], [-10, 10], [-10, 10]]
    # ylabels = ['x (m)', 'y (m)', 'z (m)', '$\phi$ (degree)', '$\\theta$ (degree)', '$\psi$ (degree)']
    # for i in range(6):
    #     plt.subplot(6, 1, i + 1)
    #     DrawDroneEstimationError(experiment_folder + '/' + dronename + '_1', i + 1, 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    #     DrawDroneEstimationError(experiment_folder + '/' + dronename + '_2', i + 1, 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    #     DrawTimeStamp(config['timestamp_part'], texty=ylims[i][1]*0.9)
    #     plt.ylim(ylims[i])
    #     plt.ylabel(ylabels[i])
    # plt.xlabel('time (s)')
    # plot = plt.subplot(6, 1, 1)
    # plot.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)
    # plt.subplots_adjust(left=0.06, right=0.99, top=0.94, bottom=0.05, hspace=0.4)
    # plt.savefig(experiment_folder + '/payload_pose_estimation_error2.png')

    ####################################################################################################

    # fig = plt.figure(figsize=(10, 2))
    # ylims = []
    # ylabels = ['x (m)', 'y (m)', 'z (m)', '$\phi$ (degree)', '$\\theta$ (degree)', '$\psi$ (degree)']
    # DrawDroneEstimationError_Position(experiment_folder + '/' + dronename + '_1', 'drone 1', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    # DrawDroneEstimationError_Position(experiment_folder + '/' + dronename + '_2', 'drone 2', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    # DrawTimeStamp(config['timestamp_part'], texty=0.2*0.9)
    # plt.ylim(0, 0.2)
    # plt.xlabel('time (s)')
    # plt.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)
    # plt.subplots_adjust(left=0.05, right=0.99, top=0.8, bottom=0.3, hspace=0.5)
    # plt.savefig(experiment_folder + '/payload_position_estimation_error.png')

    ####################################################################################################

    # meanErrors, maxErrors = DroneEstimationError(experiment_folder + '/' + dronename + '_1', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    # print('drone 1 estimation error:')
    # print('mean: ' + str(meanErrors[0]) + '\t' + str(meanErrors[1]) + '\t' + str(meanErrors[2]) + '\t' + str(meanErrors[3]) + '\t' + str(meanErrors[4]) + '\t' + str(meanErrors[5]))
    # print('max: ' + str(maxErrors[0]) + '\t' + str(maxErrors[1]) + '\t' + str(maxErrors[2]) + '\t' + str(maxErrors[3]) + '\t' + str(maxErrors[4]) + '\t' + str(maxErrors[5]))

    # meanErrors, maxErrors = DroneEstimationError(experiment_folder + '/' + dronename + '_2', startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    # print('drone 2 estimation error:')
    # print('mean: ' + str(meanErrors[0]) + '\t' + str(meanErrors[1]) + '\t' + str(meanErrors[2]) + '\t' + str(meanErrors[3]) + '\t' + str(meanErrors[4]) + '\t' + str(meanErrors[5]))
    # print('max: ' + str(maxErrors[0]) + '\t' + str(maxErrors[1]) + '\t' + str(maxErrors[2]) + '\t' + str(maxErrors[3]) + '\t' + str(maxErrors[4]) + '\t' + str(maxErrors[5]))

    # allErrors = []
    # for subid in range(10):
    #     subfolder = args.folder + '/research_experiment_' + args.id + '_' + str(subid + 1)
    #     subconfig = GetConfig(subfolder)
    #     suberrors = GetDroneEstimationError(subfolder + '/' + dronename + '_1', startTime=subconfig['startTime'], endTime=subconfig['endTime'], step=timeStep)
    #     print('estimation error: ' + str(len(suberrors)))
    #     allErrors.extend(suberrors)
    #     suberrors = GetDroneEstimationError(subfolder + '/' + dronename + '_2', startTime=subconfig['startTime'], endTime=subconfig['endTime'], step=timeStep)
    #     print('estimation error: ' + str(len(suberrors)))
    #     allErrors.extend(suberrors)
    # meanErrors, maxErrors = CalcDroneEstimationError(allErrors)
    # print('all estimation error (' + str(len(allErrors)) + '):')
    # print('mean: ' + str(meanErrors[0]) + '\t' + str(meanErrors[1]) + '\t' + str(meanErrors[2]) + '\t' + str(meanErrors[3]) + '\t' + str(meanErrors[4]) + '\t' + str(meanErrors[5]))
    # print('max: ' + str(maxErrors[0]) + '\t' + str(maxErrors[1]) + '\t' + str(maxErrors[2]) + '\t' + str(maxErrors[3]) + '\t' + str(maxErrors[4]) + '\t' + str(maxErrors[5]))

    ####################################################################################################

    if args.wind:
        fig = plt.figure(figsize=(10, 4))
        plot = plt.subplot(2, 1, 1)
        DrawWindForceChart(experiment_folder, startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
        DrawTimeStamp(config['timestamp_part'], texty=20*0.9)
        plt.ylim(0, 20)
        plt.ylabel('Speed (m/s)')
        plot = plt.subplot(2, 1, 2)
        DrawWindDirectionChart(experiment_folder, startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
        DrawTimeStamp(config['timestamp_part'], texty=180*0.9)
        plt.ylim(-180, 180)
        plt.xlabel('time (s)')
        plt.ylabel('Direction (degree)')
        plt.subplots_adjust(left=0.12, right=0.98, top=0.96, bottom=0.15, hspace=0.4)
        plt.savefig(experiment_folder + '/wind.png')

    ####################################################################################################
    
    fig = plt.figure(figsize=(10, 2))
    DrawTrajectoryError(experiment_folder, dronename, startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    DrawTimeStamp(config['timestamp_part'], texty=0.5*0.9)
    plt.ylim(0, 0.5)
    plt.ylabel('Deviation (m)')
    plt.xlabel('time (s)')
    plt.subplots_adjust(left=0.12, right=0.98, top=0.96, bottom=0.28, hspace=0)
    plt.savefig(experiment_folder + '/payload_trajectory_deviation.png')

    ####################################################################################################
    
    fig = plt.figure(figsize=(10, 2))
    meanError, maxError = DrawTrajectoryError2D(experiment_folder, dronename, startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    DrawTimeStamp(config['timestamp_part'], texty=0.5*0.9)
    plt.ylim(0, 0.5)
    plt.ylabel('Deviation (m)')
    plt.xlabel('time (s)')
    plt.subplots_adjust(left=0.12, right=0.98, top=0.96, bottom=0.28, hspace=0)
    plt.savefig(experiment_folder + '/payload_trajectory_deviation_2d.png')
    print('meanError: ' + str(meanError))
    print('maxError: ' + str(maxError))

    ####################################################################################################

    # if args.wind:
    #     fig = plt.figure(figsize=(10, 2))
    #     labels = ['$\\theta$ (degree)', '$\psi$ (degree)']
    #     for i in range(2):
    #         DrawLineChart(experiment_folder + '/' + dronename + '_1/payloadpose_truth.txt', 1 + 4 + i, labels[i], startTime=config['startTime'], endTime=config['endTime'], step=timeStep)
    #         DrawTimeStamp(config['timestamp_part'], texty=100*0.9)
    #     plt.xlabel('time (s)')
    #     plt.ylim([-100, 100])
    #     plt.ylabel('Angle (degree)')
    #     plt.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)
    #     plt.subplots_adjust(left=0.1, right=0.99, top=0.8, bottom=0.3, hspace=0.5)
    #     plt.savefig(experiment_folder + '/payload_pose_pitch_yaw.png')

    # plt.show()

    ####################################################################################################

    fig = plt.figure(figsize=(10, 2.4))
    labels = ['drone 1', 'drone 2']
    for i in range(2):
        meanAngle, maxAngle = DrawSwingAngle(dronepose[i], markerpose_truth[i], cablelength, labels[i])
    DrawTimeStamp(config['timestamp_part'], texty=20*0.9)
    plt.ylim(0, 20)
    plt.ylabel('Swing Angle (degree)')
    plt.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)
    plt.xlabel('time (s)')
    plt.subplots_adjust(left=0.12, right=0.98, top=0.76, bottom=0.26, hspace=0)
    plt.savefig(experiment_folder + '/swing_angle.png')
    print('meanAngle: ' + str(meanAngle))
    print('maxAngle: ' + str(maxAngle))

    ####################################################################################################

    # fig = plt.figure(figsize=(10, 2.4))
    # labels = ['drone 1', 'drone 2']
    # for i in range(2):
    #     DrawSwingAngle2(dronepose[i], markerpose_truth[i], cablelength, labels[i])
    # DrawTimeStamp(config['timestamp_part'], texty=10*0.9)
    # plt.ylim(0, 30)
    # plt.ylabel('Swing Angle (degree)')
    # plt.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)
    # plt.xlabel('time (s)')
    # plt.subplots_adjust(left=0.12, right=0.98, top=0.76, bottom=0.26, hspace=0)
    # plt.savefig(experiment_folder + '/swing_angle2.png')

    ####################################################################################################
