# two_drone_slung_load_paper

## Install

### Ubuntu

```
sudo apt-get install texlive-latex-base latex-cjk-all texlive-science texlive-latex-extra texmaker texlive-xetex texlive-publishers msttcorefonts
```

### Mac

```
brew cask install mactex
```

### VSCode

Install Extension: LaTeX Workshop

Setting -> latex-workshop.latex.recipes -> Edit in settings.json -> Insert

```
    "latex-workshop.latex.recipes": [
        {
            "name": "xelatex",
            "tools": [
                "xelatex"
            ]
        },
        {
            "name": "xelatex->bibtex->exlatex*2",
            "tools": [
                "xelatex",
                "bibtex",
                "xelatex",
                "xelatex"
            ]
        }
    ],
    "latex-workshop.latex.tools":[
        {
            "name":"xelatex",
            "command": "xelatex",
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "%DOC%"
            ]
        }, 
        {
            "name":"bibtex",
            "command": "bibtex",
            "args": [
                "%DOCFILE%"
            ]
        }
    ]
```